/** 
  * @file     sx1276_dlom01-s01.h
  * @author   eWBM
  * @version  0.1
  * @date     10/01/2018
  * @brief    Key and etc information is stored into Flash, whole data is encrypt using Secure storage 
  * @attention
  *      Copyright(c) 2018 eWBM Co., Ltd. www.e-wbm.com
  *
  *       This file contains information that is proprietary to
  *      eWBM Co.,Ltd. and may not be distributed or copied
  *      without written consent from eWBM Co.,Ltd.
  *
  *      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  *      ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  *      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  *      IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  *      EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
  *      GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
  *      HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHERIN CONTRACT, STRICT LIABILITY, OR TORT
  *      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  * Brief history
  * -------------
  *  10/01/2018 : Draft by eWBM 
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SX1276MB1XAS_H__
#define __SX1276MB1XAS_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

#define BOARD_WAKEUP_TIME  0 // no TCXO

#define RADIO_ANT_SWITCH_SET_TX                    1
#define RADIO_ANT_SWITCH_SET_RX                    0

#define RADIO_RF_SW_VDD_SET_ON                    1
#define RADIO_RF_SW_VDD_SET_OFF                    0


#define RF_MID_BAND_THRESH                          525000000

/* Exported functions ------------------------------------------------------- */ 

/*!
 * \brief Initializes the radio I/Os pins interface
 */
void SX1276IoInit( void );


/*!
 * \brief De-initializes the radio I/Os pins interface. 
 *
 * \remark Useful when going in MCU lowpower modes
 */
void SX1276IoDeInit( void );

/*!
 * \brief Checks if the given RF frequency is supported by the hardware
 *
 * \param [IN] frequency RF frequency to be checked
 * \retval isSupported [true: supported, false: unsupported]
 */
bool SX1276CheckRfFrequency( uint32_t frequency );

/*!
 * Radio hardware and global parameters
 */
extern SX1276_t SX1276;

#ifdef __cplusplus
}
#endif

#endif /* __SX1276MB1XAS_H__*/

/************************ (C) COPYRIGHT eWBM *****END OF FILE****/
