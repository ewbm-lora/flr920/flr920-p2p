/** 
  * @file     dlom01-s01.c 
  * @author   eWBM
  * @version  0.1
  * @date     10/01/2018
  * @brief    Key and etc information is stored into Flash, whole data is encrypt using Secure storage 
  * @attention
  *      Copyright(c) 2018 eWBM Co., Ltd. www.e-wbm.com
  *
  *       This file contains information that is proprietary to
  *      eWBM Co.,Ltd. and may not be distributed or copied
  *      without written consent from eWBM Co.,Ltd.
  *
  *      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  *      ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  *      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  *      IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  *      EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
  *      GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
  *      HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHERIN CONTRACT, STRICT LIABILITY, OR TORT
  *      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  * Brief history
  * -------------
  *  10/01/2018 : Draft by eWBM 
  */
  
/* Includes ------------------------------------------------------------------*/
#include "dlom01-s01.h"
#include <stdlib.h>

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup DLOM01_S01
  * @{
  */   
    
/** @addtogroup DLOM01_S01_LOW_LEVEL 
  * @brief This file provides set of firmware functions to manage Leds and push-button
  *        available on DLOM01_S01 Discovery Kit from STMicroelectronics.
  * @{
  */ 

/** @defgroup DLOM01_S01_LOW_LEVEL_Private_TypesDefinitions 
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup DLOM01_S01_LOW_LEVEL_Private_Defines 
  * @{
  */ 

/**
  * @brief 32L082MLM DISCO BSP Driver version number V1.0.0
  */
#define __DLOM01_S01_BSP_VERSION_MAIN   (0x01) /*!< [31:24] main version */
#define __DLOM01_S01_BSP_VERSION_SUB1   (0x00) /*!< [23:16] sub1 version */
#define __DLOM01_S01_BSP_VERSION_SUB2   (0x00) /*!< [15:8]  sub2 version */
#define __DLOM01_S01_BSP_VERSION_RC     (0x00) /*!< [7:0]  release candidate */
#define __DLOM01_S01_BSP_VERSION         ((__DLOM01_S01_BSP_VERSION_MAIN << 24)\
                                             |(__DLOM01_S01_BSP_VERSION_SUB1 << 16)\
                                             |(__DLOM01_S01_BSP_VERSION_SUB2 << 8 )\
                                             |(__DLOM01_S01_BSP_VERSION_RC))



/** @defgroup DLOM01_S01_LOW_LEVEL_Private_Variables
  * @{
  */ 
GPIO_TypeDef* LED_PORT[LEDn] = {LED1_GPIO_PORT, LED2_GPIO_PORT, LED3_GPIO_PORT, LED4_GPIO_PORT};
const uint16_t LED_PIN[LEDn] = {LED1_PIN, LED2_PIN,LED3_PIN, LED4_PIN};
                                
GPIO_TypeDef* BUTTON_PORT[BUTTONn] = {USER_BUTTON_WKUP1_GPIO_PORT, USER_BUTTON_WKUP2_GPIO_PORT}; 
const uint16_t BUTTON_PIN[BUTTONn] = {USER_BUTTON_WKUP1_PIN, USER_BUTTON_WKUP2_PIN }; 
const uint8_t BUTTON_IRQn[BUTTONn] = {USER_BUTTON_WKUP1_EXTI_IRQn, USER_BUTTON_WKUP2_EXTI_IRQn };

/**
  * @}
  */ 
  

/** @defgroup DLOM01_S01_LOW_LEVEL_Private_Functions
  * @{
  */ 
  /**
  * @}
  */
/**
  * @brief  This method returns the DLOM01_S01 BSP Driver revision
  * @param  None
  * @retval version : 0xXYZR (8bits for each decimal, R for RC)
  */
uint32_t BSP_GetVersion(void)
{
  return __DLOM01_S01_BSP_VERSION;
}

#if 1
/**
  * @brief  Configures LED GPIO.
  * @param  Led: Specifies the Led to be configured. 
  *   This parameter can be one of following parameters:
  *            @arg  LED2
  * @retval None
  */
void BSP_LED_Init(Led_TypeDef Led)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable the GPIO_LED Clock */
  LEDx_GPIO_CLK_ENABLE( Led );

  /* Configure the GPIO_LED pin */
  GPIO_InitStruct.Pin = LED_PIN[Led];
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  
  HAL_GPIO_Init(LED_PORT[Led], &GPIO_InitStruct);
}

/**
  * @brief  Turns selected LED On.
  * @param  Led: Specifies the Led to be set on. 
  *   This parameter can be one of following parameters:
  *            @arg  LED2
  * @retval None
  */
void BSP_LED_On(Led_TypeDef Led)
{
  HAL_GPIO_WritePin(LED_PORT[Led], LED_PIN[Led], GPIO_PIN_SET); 
}

/**
  * @brief  Turns selected LED Off. 
  * @param  Led: Specifies the Led to be set off. 
  *   This parameter can be one of following parameters:
  *            @arg  LED2
  * @retval None
  */
void BSP_LED_Off(Led_TypeDef Led)
{
  HAL_GPIO_WritePin(LED_PORT[Led], LED_PIN[Led], GPIO_PIN_RESET); 
}

/**
  * @brief  Toggles the selected LED.
  * @param  Led: Specifies the Led to be toggled. 
  *   This parameter can be one of following parameters:
  *            @arg  LED2
  * @retval None
  */
void BSP_LED_Toggle(Led_TypeDef Led)
{
  HAL_GPIO_TogglePin(LED_PORT[Led], LED_PIN[Led]);
}

/**
  * @brief  Configures Button GPIO and EXTI Line.
  * @param  Button: Specifies the Button to be configured.
  *   This parameter should be: BUTTON_KEY
  * @param  ButtonMode: Specifies Button mode.
  *   This parameter can be one of following parameters:   
  *     @arg  BUTTON_MODE_GPIO: Button will be used as simple IO
  *     @arg BUTTON_MODE_EXTI: Button will be connected to EXTI line with interrupt
  *                            generation capability  
  * @retval None
  */
void BSP_PB_Init(Button_TypeDef Button, ButtonMode_TypeDef ButtonMode)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  
  /* Enable the BUTTON Clock */
  BUTTONx_GPIO_CLK_ENABLE(Button);
  __HAL_RCC_SYSCFG_CLK_ENABLE();
  
  if(ButtonMode == BUTTON_MODE_GPIO)
  {
    /* Configure Button pin as input */
    GPIO_InitStruct.Pin = BUTTON_PIN[Button];
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(BUTTON_PORT[Button], &GPIO_InitStruct);
  }
  
  if(ButtonMode == BUTTON_MODE_EXTI)
  {
    /* Configure Button pin as input with External interrupt */
    GPIO_InitStruct.Pin = BUTTON_PIN[Button];
    //GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING; 
    HAL_GPIO_Init(BUTTON_PORT[Button], &GPIO_InitStruct);
    
    /* Enable and set Button EXTI Interrupt to the lowest priority */
    //NVIC_SetPriority((IRQn_Type)(BUTTON_IRQn[Button]), 0x03);
    NVIC_SetPriority((IRQn_Type)(BUTTON_IRQn[Button]), 2);
    HAL_NVIC_EnableIRQ((IRQn_Type)(BUTTON_IRQn[Button]));
  }
}

/**
  * @brief  Returns the selected Button state.
  * @param  Button: Specifies the Button to be checked.
  *   This parameter should be: BUTTON_KEY
  * @retval The Button GPIO pin value.
  */
uint32_t BSP_PB_GetState(Button_TypeDef Button)
{
  return HAL_GPIO_ReadPin(BUTTON_PORT[Button], BUTTON_PIN[Button]);
}
#endif

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */    

/**
  * @}
  */ 
    
/************************ (C) COPYRIGHT eWBM *****END OF FILE****/
