/** 
  * @file     dlom01-s01.h 
  * @author   eWBM
  * @version  0.1
  * @date     10/01/2018
  * @brief    Key and etc information is stored into Flash, whole data is encrypt using Secure storage 
  * @attention
  *      Copyright(c) 2018 eWBM Co., Ltd. www.e-wbm.com
  *
  *       This file contains information that is proprietary to
  *      eWBM Co.,Ltd. and may not be distributed or copied
  *      without written consent from eWBM Co.,Ltd.
  *
  *      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  *      ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  *      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  *      IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  *      EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
  *      GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
  *      HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHERIN CONTRACT, STRICT LIABILITY, OR TORT
  *      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  * Brief history
  * -------------
  *  10/01/2018 : Draft by eWBM 
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DLOM01_S01_H
#define __DLOM01_S01_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "stdlib.h"

/** @addtogroup BSP
  * @{
  */

/** @addtogroup DLOM01_S01
  * @{
  */

/** @addtogroup DLOM01_S01_LOW_LEVEL
  * @{
  */
      
/** @defgroup DLOM01_S01_LOW_LEVEL_Exported_Types 
  * @{
  */ 
typedef enum 
{
  LED1 = 0,
  LED_GREEN = LED1,
  LED2 = 1,
  LED_RED1 =LED2,
  LED3 = 2,
  LED_BLUE =LED3,
  LED4 = 3,
  LED_RED2 =LED4
} Led_TypeDef;

typedef enum 
{  
  BUTTON_USER = 0,
  /* Alias */
  BUTTON_KEY_WKUP1 = BUTTON_USER,
  BUTTON_KEY_WKUP2 = 1
} Button_TypeDef;

typedef enum 
{  
  BUTTON_MODE_GPIO = 0,
  BUTTON_MODE_EXTI = 1
} ButtonMode_TypeDef;

/**
  * @}
  */ 

/** @defgroup DLOM01_S01_LOW_LEVEL_Exported_Constants 
  * @{
  */ 



/** @addtogroup DLOM01_S01_LOW_LEVEL_LED
  * @{
  */
#define LEDn                               4

#define LED1_PIN                           GPIO_PIN_5
#define LED1_GPIO_PORT                     GPIOB
#define LED1_GPIO_CLK_ENABLE()           __HAL_RCC_GPIOB_CLK_ENABLE()  
#define LED1_GPIO_CLK_DISABLE()          __HAL_RCC_GPIOB_CLK_DISABLE()

#define LED2_PIN                           GPIO_PIN_5
#define LED2_GPIO_PORT                     GPIOA
#define LED2_GPIO_CLK_ENABLE()           __HAL_RCC_GPIOA_CLK_ENABLE()  
#define LED2_GPIO_CLK_DISABLE()          __HAL_RCC_GPIOA_CLK_DISABLE()

#define LED3_PIN                           GPIO_PIN_6
#define LED3_GPIO_PORT                     GPIOB
#define LED3_GPIO_CLK_ENABLE()           __HAL_RCC_GPIOB_CLK_ENABLE()  
#define LED3_GPIO_CLK_DISABLE()          __HAL_RCC_GPIOB_CLK_DISABLE()

#define LED4_PIN                           GPIO_PIN_7
#define LED4_GPIO_PORT                     GPIOB
#define LED4_GPIO_CLK_ENABLE()           __HAL_RCC_GPIOB_CLK_ENABLE()  
#define LED4_GPIO_CLK_DISABLE()          __HAL_RCC_GPIOB_CLK_DISABLE()
  
#define LEDx_GPIO_CLK_ENABLE(__INDEX__)    do { \
                                                switch( __INDEX__ ) \
                                                {\
                                                  case LED1: \
                                                    LED1_GPIO_CLK_ENABLE();   \
                                                    break;\
                                                  case LED2: \
                                                    LED2_GPIO_CLK_ENABLE();   \
                                                    break;\
                                                  case LED3: \
                                                    LED3_GPIO_CLK_ENABLE();   \
                                                    break;\
                                                  case LED4: \
                                                    LED4_GPIO_CLK_ENABLE();   \
                                                    break;\
                                                  default:\
                                                    break;\
                                                }\
                                              } while(0)
#define LEDx_GPIO_CLK_DISABLE(__INDEX__)   do { \
                                                switch( __INDEX__ ) \
                                                {\
                                                  case LED1: \
                                                    LED1_GPIO_CLK_DISABLE();   \
                                                    break;\
                                                  case LED2: \
                                                    LED2_GPIO_CLK_DISABLE();   \
                                                    break;\
                                                  case LED3: \
                                                    LED3_GPIO_CLK_DISABLE();   \
                                                    break;\
                                                  case LED4: \
                                                    LED4_GPIO_CLK_DISABLE();   \
                                                    break;\
                                                  default:\
                                                    break;\
                                                }\
                                              } while(0)
/**
  * @}
  */ 
  
/** @addtogroup DLOM01_S01_LOW_LEVEL_BUTTON
  * @{
  */  
#define BUTTONn                            2

/**
  * @brief Key push-button
  */

#define USER_BUTTON_WKUP2_PIN                         GPIO_PIN_13
#define USER_BUTTON_WKUP2_GPIO_PORT                   GPIOC
#define USER_BUTTON_WKUP2_GPIO_CLK_ENABLE()           __HAL_RCC_GPIOC_CLK_ENABLE()
#define USER_BUTTON_WKUP2_GPIO_CLK_DISABLE()          __HAL_RCC_GPIOC_CLK_DISABLE()
#define USER_BUTTON_WKUP2_EXTI_IRQn                   EXTI4_15_IRQn

#define USER_BUTTON_WKUP1_PIN                         GPIO_PIN_0
#define USER_BUTTON_WKUP1_GPIO_PORT                   GPIOA
#define USER_BUTTON_WKUP1_GPIO_CLK_ENABLE()           __HAL_RCC_GPIOA_CLK_ENABLE()   
#define USER_BUTTON_WKUP1_GPIO_CLK_DISABLE()          __HAL_RCC_GPIOA_CLK_DISABLE()  
#define USER_BUTTON_WKUP1_EXTI_LINE                   GPIO_PIN_0
#define USER_BUTTON_WKUP1_EXTI_IRQn                   EXTI0_1_IRQn
                                              
/* Aliases */
#if 0
#define KEY_BUTTON_WKUP1_PIN                          USER_BUTTON_WKUP1_PIN
#define KEY_BUTTON_WKUP1_GPIO_PORT                    USER_BUTTON_WKUP1_GPIO_PORT
#define KEY_BUTTON_WKUP1_GPIO_CLK_ENABLE()            USER_BUTTON_WKUP1_GPIO_CLK_ENABLE()
#define KEY_BUTTON_WKUP1_GPIO_CLK_DISABLE()           USER_BUTTON_WKUP1_GPIO_CLK_DISABLE()
#define KEY_BUTTON_WKUP1_EXTI_LINE                    USER_BUTTON_WKUP1_EXTI_LINE
#define KEY_BUTTON_WKUP1_EXTI_IRQn                    USER_BUTTON_WKUP1_EXTI_IRQn
#endif

#if 0
#define BUTTONx_GPIO_CLK_ENABLE(__INDEX__)     do {KEY_BUTTON_GPIO_CLK_ENABLE();  } while(0)
#define BUTTONx_GPIO_CLK_DISABLE(__INDEX__)    do {KEY_BUTTON_GPIO_CLK_DISABLE();} while(0)
#endif
#define BUTTONx_GPIO_CLK_ENABLE(__INDEX__)    do { \
                                                switch( __INDEX__ ) \
                                                {\
                                                  case BUTTON_KEY_WKUP1: \
                                                    USER_BUTTON_WKUP1_GPIO_CLK_ENABLE();   \
                                                    break;\
                                                  case BUTTON_KEY_WKUP2: \
                                                    USER_BUTTON_WKUP2_GPIO_CLK_ENABLE();   \
                                                    break;\
                                                  default:\
                                                    break;\
                                                }\
                                              } while(0)


/**
  * @}
  */ 




/** @defgroup DLOM01_S01_LOW_LEVEL_Exported_Functions 
  * @{
  */
uint32_t         BSP_GetVersion(void);  
void             BSP_LED_Init(Led_TypeDef Led);
void             BSP_LED_On(Led_TypeDef Led);
void             BSP_LED_Off(Led_TypeDef Led);
void             BSP_LED_Toggle(Led_TypeDef Led);                 
void             BSP_PB_Init(Button_TypeDef Button, ButtonMode_TypeDef Button_Mode);
uint32_t         BSP_PB_GetState(Button_TypeDef Button);                

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */ 
    
#ifdef __cplusplus
}
#endif

#endif /* __DLOM01_S01_H */

/************************ (C) COPYRIGHT eWBM *****END OF FILE****/

