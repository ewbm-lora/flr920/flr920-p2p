/** 
  * @file     sx1276_dlom01-s01.c 
  * @author   eWBM
  * @version  0.1
  * @date     10/01/2018
  * @brief    Key and etc information is stored into Flash, whole data is encrypt using Secure storage 
  * @attention
  *      Copyright(c) 2018 eWBM Co., Ltd. www.e-wbm.com
  *
  *       This file contains information that is proprietary to
  *      eWBM Co.,Ltd. and may not be distributed or copied
  *      without written consent from eWBM Co.,Ltd.
  *
  *      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  *      ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  *      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  *      IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  *      EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
  *      GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
  *      HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHERIN CONTRACT, STRICT LIABILITY, OR TORT
  *      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  * Brief history
  * -------------
  *  10/01/2018 : Draft by eWBM 
  */
  
/* Includes ------------------------------------------------------------------*/

#include "hw.h"
#include "radio.h"
#include "sx1276.h"
#include "sx1276_dlom01-s01.h"


#define IRQ_HIGH_PRIORITY  0

#define RF_SW_VDD_AON 0


// Flyvo 에서 양산시에 설정한다.
// STI SDK 릴리즈 컴파일시에는 이 부분을 막는다.
#define FEATURE_FLYVO_DEVICE





/*!
 * Flag used to set the RF switch control pins in low power mode when the radio is not active.
 */
static bool RadioIsActive = false;

static void SX1276AntSwInit( void );

static void SX1276AntSwDeInit( void );

void SX1276SetXO( uint8_t state );

uint32_t SX1276GetWakeTime( void );

void SX1276IoIrqInit( DioIrqHandler **irqHandlers );

uint8_t SX1276GetPaSelect( uint32_t channel );

void SX1276SetAntSwLowPower( bool status );

void SX1276SetRfTxPower( int8_t power );

void SX1276SetAntSw( uint8_t opMode );
/*!
 * \brief Controls the antena switch if necessary.
 *
 * \remark see errata note
 *
 * \param [IN] opMode Current radio operating mode
 */
static LoRaBoardCallback_t BoardCallbacks = { SX1276SetXO,
                                              SX1276GetWakeTime,
                                              SX1276IoIrqInit,
                                              SX1276SetRfTxPower,
                                              SX1276SetAntSwLowPower,
                                              SX1276SetAntSw};

/*!
 * Radio driver structure initialization
 */
const struct Radio_s Radio =
{
    SX1276IoInit,
    SX1276IoDeInit,
    SX1276Init,
    SX1276GetStatus,
    SX1276SetModem,
    SX1276SetChannel,
    SX1276IsChannelFree,
    SX1276Random,
    SX1276SetRxConfig,
    SX1276SetTxConfig,
    SX1276CheckRfFrequency,
    SX1276GetTimeOnAir,
    SX1276Send,
    SX1276SetSleep,
    SX1276SetStby,
    SX1276SetRx,
    SX1276StartCad,
    SX1276SetTxContinuousWave,
    SX1276ReadRssi,
    SX1276Write,
    SX1276Read,
    SX1276WriteBuffer,
    SX1276ReadBuffer,
    SX1276SetMaxPayloadLength,
    SX1276SetPublicNetwork,
    SX1276GetRadioWakeUpTime
};



void callback_sleep_key1(void)
{
    PRINTF("callback_sleep_key1 \r\n");
}

extern void callback_sleep_key2(void)
{
    PRINTF("callback_sleep_key2 \r\n");
}



uint32_t SX1276GetWakeTime( void )
{
  return  0;  // willyhong 2019_11_12BOARD_WAKEUP_TIME;
}

void SX1276SetXO( uint8_t state )
{
  HW_RTC_DelayMs(2);  //willyhong_2019_11_12
}
void SX1276IoInit( void )
{
  GPIO_InitTypeDef initStruct={0};
  
  SX1276BoardInit( &BoardCallbacks );
  
  initStruct.Mode = GPIO_MODE_IT_RISING;
  initStruct.Pull = GPIO_PULLDOWN;
  initStruct.Speed = GPIO_SPEED_HIGH;

  HW_GPIO_Init( RADIO_DIO_0_PORT, RADIO_DIO_0_PIN, &initStruct );
  HW_GPIO_Init( RADIO_DIO_1_PORT, RADIO_DIO_1_PIN, &initStruct );
  HW_GPIO_Init( RADIO_DIO_2_PORT, RADIO_DIO_2_PIN, &initStruct );
  HW_GPIO_Init( RADIO_DIO_3_PORT, RADIO_DIO_3_PIN, &initStruct );

#ifdef FEATURE_FLYVO_DEVICE
  HW_GPIO_Init( GPIOA, GPIO_PIN_0, &initStruct );
  HW_GPIO_Init( GPIOC, GPIO_PIN_13, &initStruct );
#else  
  // ONLY STI-SDK-MODE
  HW_GPIO_Init( GPIOA, GPIO_PIN_3, &initStruct );
  HW_GPIO_Init( GPIOB, GPIO_PIN_11, &initStruct );
#endif


 
  initStruct.Mode = GPIO_MODE_OUTPUT_PP;
  initStruct.Pull = GPIO_PULLUP;
  initStruct.Speed = GPIO_SPEED_HIGH;

  HW_GPIO_Init( RADIO_RF_SW_VDD_PORT, RADIO_RF_SW_VDD_PIN, &initStruct  );
  HW_GPIO_Write( RADIO_RF_SW_VDD_PORT, RADIO_RF_SW_VDD_PIN, RADIO_RF_SW_VDD_SET_ON);
  

}

void SX1276IoIrqInit( DioIrqHandler **irqHandlers )
{
  HW_GPIO_SetIrq( RADIO_DIO_0_PORT, RADIO_DIO_0_PIN, IRQ_HIGH_PRIORITY, irqHandlers[0] );
  HW_GPIO_SetIrq( RADIO_DIO_1_PORT, RADIO_DIO_1_PIN, IRQ_HIGH_PRIORITY, irqHandlers[1] );
  HW_GPIO_SetIrq( RADIO_DIO_2_PORT, RADIO_DIO_2_PIN, IRQ_HIGH_PRIORITY, irqHandlers[2] );
  HW_GPIO_SetIrq( RADIO_DIO_3_PORT, RADIO_DIO_3_PIN, IRQ_HIGH_PRIORITY, irqHandlers[3] );

#ifdef FEATURE_FLYVO_DEVICE
  HW_GPIO_SetIrq( GPIOA, GPIO_PIN_0, IRQ_HIGH_PRIORITY, callback_sleep_key1);
  HW_GPIO_SetIrq( GPIOC, GPIO_PIN_13, IRQ_HIGH_PRIORITY, callback_sleep_key2);
#else
  // ONLY STI-SDK-MODE
  HW_GPIO_SetIrq( GPIOA, GPIO_PIN_3, IRQ_HIGH_PRIORITY, callback_sleep_key1);
  HW_GPIO_SetIrq( GPIOB, GPIO_PIN_11, IRQ_HIGH_PRIORITY, callback_sleep_key2);
#endif
}

void SX1276IoDeInit( void )
{
  GPIO_InitTypeDef initStruct={0};

  initStruct.Mode = GPIO_MODE_IT_RISING ;
  initStruct.Pull = GPIO_PULLDOWN;
  
  HW_GPIO_Init( RADIO_DIO_0_PORT, RADIO_DIO_0_PIN, &initStruct );
  HW_GPIO_Init( RADIO_DIO_1_PORT, RADIO_DIO_1_PIN, &initStruct );
  HW_GPIO_Init( RADIO_DIO_2_PORT, RADIO_DIO_2_PIN, &initStruct );
  HW_GPIO_Init( RADIO_DIO_3_PORT, RADIO_DIO_3_PIN, &initStruct );

#ifdef FEATURE_FLYVO_DEVICE
  HW_GPIO_Init( GPIOA, GPIO_PIN_0, &initStruct );
  HW_GPIO_Init( GPIOC, GPIO_PIN_13, &initStruct );
#else
  // ONLY STI-SDK-MODE
  HW_GPIO_Init( GPIOA, GPIO_PIN_3, &initStruct );
  HW_GPIO_Init( GPIOB, GPIO_PIN_11, &initStruct );
#endif
}

void SX1276SetRfTxPower( int8_t power )
{
    uint8_t paConfig = 0;
    uint8_t paDac = 0;

    paConfig = SX1276Read( REG_PACONFIG );
    paDac = SX1276Read( REG_PADAC );
//PRINTF("%s(%d) PACONFIG %x PADAC %x\r\n", __func__, power, paConfig, paDac);

    paConfig = ( paConfig & RF_PACONFIG_PASELECT_MASK ) | SX1276GetPaSelect( SX1276.Settings.Channel );
    paConfig = ( paConfig & RF_PACONFIG_MAX_POWER_MASK ) | 0x70;

    if( ( paConfig & RF_PACONFIG_PASELECT_PABOOST ) == RF_PACONFIG_PASELECT_PABOOST )
    {
        if( power > 17 )
        {
            paDac = ( paDac & RF_PADAC_20DBM_MASK ) | RF_PADAC_20DBM_ON;
        }
        else
        {
            paDac = ( paDac & RF_PADAC_20DBM_MASK ) | RF_PADAC_20DBM_OFF;
        }
        if( ( paDac & RF_PADAC_20DBM_ON ) == RF_PADAC_20DBM_ON )
        {
            if( power < 5 )
            {
                power = 5;
            }
            if( power > 20 )
            {
                power = 20;
            }
            paConfig = ( paConfig & RF_PACONFIG_OUTPUTPOWER_MASK ) | ( uint8_t )( ( uint16_t )( power - 5 ) & 0x0F );
        }
        else
        {
            if( power < 2 )
            {
                power = 2;
            }
            if( power > 17 )
            {
                power = 17;
            }
            paConfig = ( paConfig & RF_PACONFIG_OUTPUTPOWER_MASK ) | ( uint8_t )( ( uint16_t )( power - 2 ) & 0x0F );
        }
    }
    else
    {
        if( power < -1 )
        {
            power = -1;
        }
        if( power > 14 )
        {
            power = 14;
        }
        paConfig = ( paConfig & RF_PACONFIG_OUTPUTPOWER_MASK ) | ( uint8_t )( ( uint16_t )( power + 1 ) & 0x0F );
    }
    SX1276Write( REG_PACONFIG, paConfig );
    SX1276Write( REG_PADAC, paDac );
}

uint8_t SX1276GetPaSelect( uint32_t channel )
{
#if 1 // eWBM_bkjun: PABOOST circuit is not designed for DLOM01S1
    return RF_PACONFIG_PASELECT_RFO;
    //return RF_PACONFIG_PASELECT_PABOOST;
#else
    if( channel < RF_MID_BAND_THRESH )
    {
        return RF_PACONFIG_PASELECT_RFO;
    }
    else
    {
        return RF_PACONFIG_PASELECT_PABOOST;
    }
#endif
}

void SX1276SetAntSwLowPower( bool status )
{
    if( RadioIsActive != status )
    {
        RadioIsActive = status;

        if( status == false )
        {
            SX1276AntSwInit( );
        }
        else
        {
            SX1276AntSwDeInit( );
        }
    }
}

static void SX1276AntSwInit( void )
{
  GPIO_InitTypeDef initStruct={0};

//PRINTF("%s()\r\n", __func__);

  initStruct.Mode =GPIO_MODE_OUTPUT_PP;
  initStruct.Pull = GPIO_NOPULL;
  initStruct.Speed = GPIO_SPEED_HIGH;
  
  HW_GPIO_Init( RADIO_ANT_SWITCH_PORT, RADIO_ANT_SWITCH_PIN, &initStruct  );
  HW_GPIO_Write( RADIO_ANT_SWITCH_PORT, RADIO_ANT_SWITCH_PIN, RADIO_ANT_SWITCH_SET_RX);

#if 0 //!RF_SW_VDD_AON
  HW_GPIO_Init( RADIO_RF_SW_VDD_PORT, RADIO_RF_SW_VDD_PIN, &initStruct  );
  HW_GPIO_Write( RADIO_RF_SW_VDD_PORT, RADIO_RF_SW_VDD_PIN, RADIO_RF_SW_VDD_SET_ON);
#endif
}

static void SX1276AntSwDeInit( void )
{
  GPIO_InitTypeDef initStruct={0};

//PRINTF("%s()\r\n", __func__);

  initStruct.Mode = GPIO_MODE_OUTPUT_PP ;
  
  initStruct.Pull = GPIO_NOPULL;
  initStruct.Speed = GPIO_SPEED_HIGH;

  HW_GPIO_Init(  RADIO_ANT_SWITCH_PORT, RADIO_ANT_SWITCH_PIN, &initStruct );
  HW_GPIO_Write( RADIO_ANT_SWITCH_PORT, RADIO_ANT_SWITCH_PIN, 0);

#if 0 // !RF_SW_VDD_AON
  HW_GPIO_Init( RADIO_RF_SW_VDD_PORT, RADIO_RF_SW_VDD_PIN, &initStruct  );
  HW_GPIO_Write( RADIO_RF_SW_VDD_PORT, RADIO_RF_SW_VDD_PIN, RADIO_RF_SW_VDD_SET_OFF);
#endif
}

void SX1276SetAntSw( uint8_t opMode )
{
    switch( opMode )
    {
    case RFLR_OPMODE_TRANSMITTER:
        HW_GPIO_Write( RADIO_ANT_SWITCH_PORT, RADIO_ANT_SWITCH_PIN, RADIO_ANT_SWITCH_SET_TX);
        break;
    case RFLR_OPMODE_RECEIVER:
    case RFLR_OPMODE_RECEIVER_SINGLE:
    case RFLR_OPMODE_CAD:
    default:
        HW_GPIO_Write( RADIO_ANT_SWITCH_PORT, RADIO_ANT_SWITCH_PIN, RADIO_ANT_SWITCH_SET_RX);
        break;
    }
}

bool SX1276CheckRfFrequency( uint32_t frequency )
{
    // Implement check. Currently all frequencies are supported
    return true;
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
