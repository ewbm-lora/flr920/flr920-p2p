/*!
 * \file      timer.c
 *
 * \brief     Timer objects and scheduling management implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 */
#include "timer.h"
#include "hw_rtc.h"
#include "rtc_mktime.h"

/*!
 * \brief Read the timer value of the currently running timer
 *
 * \retval value current timer value
 */
TimerTime_t TimerGetValue( void );

TimerSysTime_t DeltaSysTime = { 0 };

void TimerSetSysTime( TimerSysTime_t sysTime )
{
    RtcSetSysTime( sysTime.Seconds, sysTime.SubSeconds );
}

TimerSysTime_t TimerGetSysTime( void )
{
    TimerSysTime_t sysTime = { 0 };

    sysTime.Seconds = RtcGetSysTime( ( uint16_t* )&sysTime.SubSeconds );

    return sysTime;
}

bool GetGPSTime( uint32_t seconds, uint16_t subSeconds, struct tm *pTimeinfo)
{
    struct tm timeinfo;

    // Convert the time into a tm
    if( _rtc_localtime( ( time_t )seconds, &timeinfo ) == false )
    {
        return false;
    }

    timeinfo.tm_mon++;
#if defined(SKTELECOM)
    timeinfo.tm_year = ( timeinfo.tm_year >= 100 ) ? timeinfo.tm_year - 100 : timeinfo.tm_year;
#elif defined(SKTELESYS) // STI requested time format.
    timeinfo.tm_year += 1900;
#endif
    *pTimeinfo = timeinfo;

    //DBG_PRINTF("Date %d-%d-%d, Time %d:%d:%d \r\n", pTimeinfo->tm_year, pTimeinfo->tm_mon, pTimeinfo->tm_mday, pTimeinfo->tm_hour, pTimeinfo->tm_min, pTimeinfo->tm_sec);
    return true;
}

