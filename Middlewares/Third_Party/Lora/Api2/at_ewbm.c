/*******************************************************************************
 * @file    at.c
 * @author  MCD Application Team
 * @version V1.1.4
 * @date    08-January-2018
 * @brief   at command API
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V.
 * All rights reserved.</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "at_ewbm.h"
#include "utilities.h"

#include "radio.h"
#include "vcom.h"
#include "tiny_sscanf.h"
#include "version.h"
#include "app.h"
#include "ewbm_lora_storage.h"



/* External variables --------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/**
 * @brief Max size of the data that can be received
 */
#define MAX_RECEIVED_DATA 128


static sAppInfoType*            g_pAppInfo = NULL;
static uint8_t                  g_tempBuff[50];






void reset_reason(uint32_t reason)
{
    uint32_t    *memory_ptr;
    memory_ptr = ((uint32_t*)(SRAM_BASE + SRAM_SIZE_MAX) - 4);
    *memory_ptr = reason;
}



static void scanf_string_hhx(const char *form, uint8_t *pt, uint8_t size)
{
    uint8_t  i;
    for (i=0; i < size; i++) {
        char hex[3];
        hex[0] = form[i*2];
        hex[1] = form[(i*2)+1];
        hex[2] = 0;

        pt[i] = strtol(hex, NULL, 16);
    }
}

static void print_8_02x(const char *title, uint8_t *pt)
{
  AT_PRINTF("[%s=%02x%02x%02x%02x%02x%02x%02x%02x]\r\n",
            title,
            pt[0], pt[1], pt[2], pt[3], pt[4], pt[5], pt[6], pt[7]);
}

static void print_16_02x(const char *tltle, uint8_t *pt)
{
  AT_PRINTF("[%s=%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x]\r\n",
            tltle,
            pt[0], pt[1], pt[2], pt[3],
            pt[4], pt[5], pt[6], pt[7],
            pt[8], pt[9], pt[10], pt[11],
            pt[12], pt[13], pt[14], pt[15]);
}



void SetATCommandInfo(void *pInfo)
{
    g_pAppInfo = (sAppInfoType*)pInfo;
}


void* GetATCommandInfo(void)
{
    return (void*)g_pAppInfo;
}


ATEerror_t at_return_ok(const char *param)
{
  return AT_OK;
}

ATEerror_t at_return_error(const char *param)
{
  return AT_ERROR;
}




ATEerror_t at_get_dev_eui(const char *param)
{
    memset(g_tempBuff, NULL, sizeof(g_tempBuff));
    ewbm_lora_storage_readb(LORA_U08_DEVEUI, g_tempBuff);
    print_8_02x("DEVEUI", g_tempBuff);
    return AT_OK;
}

ATEerror_t at_set_dev_eui(const char *param)
{
    if (strlen(param) != 16) {
        return AT_PARAM_ERROR;
    }

    scanf_string_hhx(param, g_tempBuff, 8);

    ewbm_lora_storage_writeb(LORA_U08_DEVEUI, g_tempBuff);
    print_8_02x("DEVEUI", g_tempBuff);
    
    return AT_OK;
}




ATEerror_t at_get_aes_pwd(const char *param)
{
    memset(g_tempBuff, NULL, sizeof(g_tempBuff));
    ewbm_lora_storage_readb(LORA_U08_AES_PWD, g_tempBuff);
    print_16_02x("AESPWD", g_tempBuff);
    return AT_OK;
}

ATEerror_t at_set_aes_pwd(const char *param)
{
    if (strlen(param) != 32) {
        return AT_PARAM_ERROR;
    }

    scanf_string_hhx(param, g_tempBuff, 16);

    ewbm_lora_storage_writeb(LORA_U08_AES_PWD, g_tempBuff);

    print_16_02x("AESPWD", g_tempBuff);
    return AT_OK;
}





ATEerror_t at_get_p2p_dbgl(const char *param)
{
    AT_PRINTF("[DBGL=%d]\r\n", g_pAppInfo->u8Dbgl);
    return AT_OK;

}


ATEerror_t at_set_p2p_dbgl(const char *param)
{
    uint8_t u8Dbgl;
        
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%u", &u8Dbgl) != 1) {
            return AT_ERROR;
        }

        AT_PRINTF("[DBGL=%d]\r\n", u8Dbgl);
        g_pAppInfo->u8Dbgl = !!u8Dbgl;
        ewbm_lora_storage_writeb(LORA_U08_DBGL, &g_pAppInfo->u8Dbgl);
        return AT_OK;
    }
   
    return AT_PARAM_ERROR;

}



ATEerror_t at_get_p2p_channel(const char *param)
{
    AT_PRINTF("[CHANNEL=%d]\r\n", g_pAppInfo->u8Channel);
    return AT_OK;
}



ATEerror_t at_set_p2p_channel(const char *param)
{
    uint8_t u8ChannelIndex;
    
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%u", &u8ChannelIndex) != 1) {
            return AT_ERROR;
        }
        if(u8ChannelIndex > 31) {
            return AT_PARAM_ERROR;
        }

        AT_PRINTF("[CHANNEL=%d]\r\n", u8ChannelIndex);
        if(g_pAppInfo->SetChannel) {
            return g_pAppInfo->SetChannel(u8ChannelIndex);
        }
    }
   
    return AT_PARAM_ERROR;
}




ATEerror_t at_get_p2p_rateset(const char *param)
{
    AT_PRINTF("[RATESET=%d]\r\n", g_pAppInfo->u8Rateset);
    return AT_OK;
}



ATEerror_t at_set_p2p_rateset(const char *param)
{
    uint8_t u8Rateset;
    
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%u", &u8Rateset) != 1) {
            return AT_ERROR;
        }

        if(u8Rateset > 5) {
            return AT_PARAM_ERROR;
        }

        AT_PRINTF("[RATESET=%d]\r\n", u8Rateset);
        if(g_pAppInfo->SetRateset) {
            return g_pAppInfo->SetRateset(u8Rateset);
        }
    }
   
    return AT_PARAM_ERROR;
}




ATEerror_t at_get_p2p_pan_id(const char *param)
{
    AT_PRINTF("[PAN ID=%d]\r\n", g_pAppInfo->u16PanID);
    return AT_OK;
}



ATEerror_t at_set_p2p_pan_id(const char *param)
{
    uint16_t u16PanID;
    
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%u", &u16PanID) != 1) {
            return AT_ERROR;
        }
        g_pAppInfo->u16PanID = u16PanID;
        AT_PRINTF("[PAN ID=%d]\r\n", g_pAppInfo->u16PanID);
        ewbm_lora_storage_writew(LORA_U16_PANID, g_pAppInfo->u16PanID);
        return AT_OK;
    }
    return AT_PARAM_ERROR;
}



ATEerror_t at_get_p2p_node_id(const char *param)
{
    AT_PRINTF("[NODE ID=%d]\r\n", g_pAppInfo->u16NodeID);
    return AT_OK;
}



ATEerror_t at_set_p2p_node_id(const char *param)
{
    uint16_t u16NodeID;
    
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%u", &u16NodeID) != 1) {
            return AT_ERROR;
        }
        g_pAppInfo->u16NodeID = u16NodeID;
        AT_PRINTF("[NODE ID=%d]\r\n", g_pAppInfo->u16NodeID);
        ewbm_lora_storage_writew(LORA_U16_NODEID, g_pAppInfo->u16NodeID);
        return AT_OK;
    }
    return AT_PARAM_ERROR;
}




ATEerror_t at_get_p2p_null_modem(const char *param)
{
    AT_PRINTF("[NULL MODEM=%d]\r\n", g_pAppInfo->u8NullModem);
    return AT_OK;
}



ATEerror_t at_set_p2p_null_modem(const char *param)
{
    uint8_t u8NullModem;
    
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%u", &u8NullModem) != 1) {
            return AT_ERROR;
        }

        if(u8NullModem != 0 && u8NullModem != 1) {
            return AT_PARAM_ERROR;
        }
        
        g_pAppInfo->u8NullModem = u8NullModem;
        AT_PRINTF("[NULL MODEM=%d]\r\n", g_pAppInfo->u8NullModem);
        ewbm_lora_storage_writeb(LORA_U08_NULLMODEM, &g_pAppInfo->u8NullModem);
        return AT_OK;
    }
    
    return AT_PARAM_ERROR;

    
}




ATEerror_t at_set_p2p_energy_scan(const char *param)
{
    uint32_t u32EnergyScan;
    
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%lu", &u32EnergyScan) != 1) {
            return AT_ERROR;
        }

        if(g_pAppInfo->SetEnergyScan != NULL) {
            return g_pAppInfo->SetEnergyScan(u32EnergyScan);
        }
    }
    
    return AT_ERROR;
    
}




ATEerror_t at_set_p2p_energy_scan_all(const char *param)
{
    uint32_t u32EnergyScan;
    
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%lu", &u32EnergyScan) != 1) {
            return AT_ERROR;
        }

        if(g_pAppInfo->SetEnergyScanAll != NULL) {
            return g_pAppInfo->SetEnergyScanAll(u32EnergyScan);
        }
    }
    
    return AT_ERROR;
    
}


ATEerror_t at_set_p2p_help(const char *param)
{
    AT_PRINTF("[AT+CHANNEL=0...31, - set the current channel]\r\n");
    AT_PRINTF("[AT+PAN_ID, - view the current PAN ID]\r\n");
    AT_PRINTF("[AT+PAN_ID=0...9999, set the current PAN ID]\r\n");
    AT_PRINTF("[AT+NODE_ID, - view the current node ID]\r\n");
    AT_PRINTF("[AT+AES128, - view the setting of AES128 encryption]\r\n");
    AT_PRINTF("[AT+AES128=ON|OFF, - set the AES128 encryption function ON or OFF]\r\n");
    AT_PRINTF("[AT+PING=dest,len,interval,count, - test ping with specified parameters]\r\n");
    AT_PRINTF("[AT+FWVER, - view the current firmware version]\r\n");
    AT_PRINTF("[AT+RESET, - modem reset]\r\n");
    AT_PRINTF("[AT+FSET, - factory settings]\r\n");
    
    return AT_OK;
}


ATEerror_t at_set_p2p_reset(const char *param)
{
    reset_reason(0);   


    if(g_pAppInfo->SetReset != NULL) {
        return g_pAppInfo->SetReset();
    }
    return AT_ERROR;
}



ATEerror_t at_set_p2p_fset(const char *param)
{
    reset_reason(0);
    
    if(g_pAppInfo->SetFSet != NULL) {
        return g_pAppInfo->SetFSet();
    }
    return AT_ERROR;
}


ATEerror_t at_get_p2p_rfpower(const char *param)
{
    AT_PRINTF("[RFPOWER=%d]\r\n", g_pAppInfo->u8Power);
    return AT_OK;
}


ATEerror_t at_set_p2p_rfpower(const char *param)
{
    uint8_t u8Power;
    if(param == NULL || param[0] == NULL) {
        return AT_PARAM_ERROR;
    }

    if (tiny_sscanf(param, "%u", &u8Power) != 1) {
        return AT_PARAM_ERROR;
    }

    if(u8Power > 7) return AT_PARAM_ERROR;

    
    g_pAppInfo->u8Power = u8Power;
    AT_PRINTF("[RFPOWER=%d]\r\n", g_pAppInfo->u8Power);
    ewbm_lora_storage_writeb(LORA_U08_POWER, &g_pAppInfo->u8Power);
    return AT_OK;
}


ATEerror_t at_get_p2p_fwver(const char *param)
{
    if(g_pAppInfo->u16NodeID ==0) {
        AT_PRINTF("[F/W VER = Master %s]\r\n", g_pAppInfo->sVersion);
    }
    else {
        AT_PRINTF("[F/W VER = Slave %s]\r\n", g_pAppInfo->sVersion);
    }
    return AT_OK;
}


ATEerror_t at_get_p2p_aes128(const char *param)
{
    if(g_pAppInfo->u8Aes128) {
        AT_PRINTF("[AES128 ON]\r\n");
    }
    else {
        AT_PRINTF("[AES128 OFF]\r\n");
    }
    return AT_OK;
}



ATEerror_t at_set_p2p_aes128(const char *param)
{
    if(param != NULL && param[0] != NULL) {
        if (param[0] == 'O' && param[1] == 'N') 
        {
            g_pAppInfo->u8Aes128 = true;
            AT_PRINTF("[AES128 ON]\r\n");
        }
        else {
            g_pAppInfo->u8Aes128 = false;
            AT_PRINTF("[AES128 OFF]\r\n");
        }
        ewbm_lora_storage_writeb(LORA_U08_AES128, &g_pAppInfo->u8Aes128);
        return AT_OK;
    }
    return AT_PARAM_ERROR;
}




ATEerror_t at_set_p2p_ping(const char *param)
{
    uint16_t    destNode;
    uint8_t     len;
    uint32_t    interval;
    uint32_t    count;
    
    if(param == NULL || param[0] == NULL) {
        return AT_PARAM_ERROR;
    }

    if (tiny_sscanf(param, "%u,%u,%lu,%lu", &destNode, &len, &interval, &count) != 4) {
        return AT_PARAM_ERROR;
    }

    if(len > 128) return AT_PARAM_ERROR;
    if(interval > 60) return AT_PARAM_ERROR;
    if(count > 99999999) return AT_PARAM_ERROR;


    if(g_pAppInfo->SetPing) {
       return g_pAppInfo->SetPing(destNode, len, interval*1000, count); 
    }
    else {
        return AT_ERROR;
    }
}





ATEerror_t at_get_p2p_baudrate(const char *param)
{
    AT_PRINTF("[BAUDRATE=%d]\r\n", g_pAppInfo->u32Baudrate);
    return AT_OK;

}



ATEerror_t at_set_p2p_baudrate(const char *param)
{
    uint32_t u32Baudrate;
    if (tiny_sscanf(param, "%lu", &u32Baudrate) != 1) {
        return AT_ERROR;
    }

    switch(u32Baudrate) {
        case 9600 :
        case 19200 :
        case 38400 :
        case 57600 :
        case 115200 :
        case 230400 :
            AT_PRINTF("[BAUDRATE=%d]\r\n", u32Baudrate);
            if(g_pAppInfo->SetBaudrate) {
                return g_pAppInfo->SetBaudrate(u32Baudrate);
            }
            
        default :
            break;
    }
    return AT_PARAM_ERROR;
}



ATEerror_t at_set_p2p_send(const char *param)
{
    char        sToken[50];
    uint16_t    destNode;
    int         size;
    uint8_t     len;
  
    if(param == NULL || param[0] == NULL) {
        return AT_PARAM_ERROR;
    }

    char *sStart = (char*)param;
    char *sParse = strstr(sStart, ",");
    if(sParse == NULL) {
        return AT_PARAM_ERROR;
    }

    size = (uint8_t)(sParse - sStart);
    memcpy(sToken, sStart, size);
    sToken[size] = NULL;
    destNode = atoi(sToken);
    

    sStart += (size + 1);
    sParse = strstr(sStart, ",");
    if(sParse == NULL) {
        return AT_PARAM_ERROR;
    }
    size = (uint8_t)(sParse - sStart);
    memcpy(sToken, sStart, size);
    sToken[size] = NULL;
    len = atoi(sToken);
    

    sStart += (size + 1);
    memcpy(g_pAppInfo->sTxBuff, sStart, len);
    
    g_pAppInfo->sTxBuff[len] = NULL;
    
    if(g_pAppInfo->SetSend != NULL) {
       g_pAppInfo->SetSend(destNode, len, g_pAppInfo->sTxBuff, g_pAppInfo->u8NullModem, 0); 
       return AT_OK;
    }

    return AT_ERROR;

}



ATEerror_t at_set_p2p_send_all(const char *param, bool isGaro, uint32_t size)
{
    /* Master Node */

    if(g_pAppInfo->u8NullModem == 0) {
        return AT_ERROR;
    }

    if(g_pAppInfo->u16NodeID == 0) {
        if(g_pAppInfo->SetSend != NULL) {
           return g_pAppInfo->SetSend(0xFFFF, size, (char *)param, 1, isGaro); 
        }
    }
    else {
        if(g_pAppInfo->SetSend != NULL) {
           return g_pAppInfo->SetSend(0, size, (char*)param, 1, isGaro); 
        }
    }
    return AT_ERROR;

}



ATEerror_t at_boot_download(const char *param)
{
    reset_reason(0xFF05FF04);

    /* BOOT */
    g_pAppInfo->SetReset();
    return AT_OK;

}



ATEerror_t at_get_hop_level(const char *param)
{
    AT_PRINTF("[HOPLEVEL=%d]\r\n", g_pAppInfo->u8HopLevel);
    return AT_OK;

}


ATEerror_t at_set_hop_level(const char *param)
{
    uint8_t u8HopLevel;
    if(param == NULL || param[0] == NULL) {
        return AT_PARAM_ERROR;
    }

    if (tiny_sscanf(param, "%u", &u8HopLevel) != 1) {
        return AT_PARAM_ERROR;
    }

    if(u8HopLevel > 254) return AT_PARAM_ERROR;

    
    g_pAppInfo->u8HopLevel = u8HopLevel;
    AT_PRINTF("[HOPLEVEL=%d]\r\n", u8HopLevel);
    ewbm_lora_storage_writeb(LORA_U08_HOP_LEVEL, &g_pAppInfo->u8HopLevel);
    return AT_OK;

}


ATEerror_t at_get_hop_delay(const char *param)
{
    AT_PRINTF("[HOPDELAY=%d]\r\n", g_pAppInfo->u32HopDelay);
    return AT_OK;

}

/* Second */
ATEerror_t at_set_hop_delay(const char *param)
{
    uint32_t u32HopDelay;
    
    if(param == NULL || param[0] == NULL) {
        return AT_PARAM_ERROR;
    }

    if (tiny_sscanf(param, "%lu", &u32HopDelay) != 1) {
        return AT_PARAM_ERROR;
    }

    if(u32HopDelay > (30*1000)) return AT_PARAM_ERROR;
    
    g_pAppInfo->u32HopDelay = u32HopDelay;
    AT_PRINTF("[HOPDELAY=%d]\r\n", u32HopDelay);
    ewbm_lora_storage_writedw(LORA_U32_HOP_DELAY, g_pAppInfo->u32HopDelay);
    return AT_OK;
}



ATEerror_t at_set_sleep(const char *param)
{
    uint32_t nSecond;
    
    if(param == NULL || param[0] == NULL) {
        return AT_PARAM_ERROR;
    }
    
    if (tiny_sscanf(param, "%lu", &nSecond) != 1) {
        return AT_PARAM_ERROR;
    }

    if(nSecond > (60*60*24*29)) {
        return AT_PARAM_ERROR;
    }
   

    AT_PRINTF("[SLEEP=%d]\r\n", nSecond);

    return g_pAppInfo->SetSleep(nSecond);
    
}


ATEerror_t at_get_rxdata(const char *param)
{
    return g_pAppInfo->GetRxData();
}




ATEerror_t at_get_recv_fmt(const char *param)
{
    AT_PRINTF("[RECVFMT=%d]\r\n", g_pAppInfo->u8RecvFormat);
    return AT_OK;

}


ATEerror_t at_set_recv_fmt(const char *param)
{
    uint8_t u8Format;
    
    if(param == NULL || param[0] == NULL) {
        return AT_PARAM_ERROR;
    }

    if (tiny_sscanf(param, "%u", &u8Format) != 1) {
        return AT_PARAM_ERROR;
    }

    if(u8Format > 1) return AT_PARAM_ERROR;
    
    g_pAppInfo->u8RecvFormat = u8Format;
    AT_PRINTF("[RECVFMT=%d]\r\n", u8Format);
    ewbm_lora_storage_writeb(LORA_U08_RECV_FORMAT, &g_pAppInfo->u8RecvFormat);
    
    return AT_OK;
}




ATEerror_t at_get_tick(const char *param)
{
    AT_PRINTF("[TICK=%d]\r\n", (int)HAL_GetTick());
    return AT_OK;
}


