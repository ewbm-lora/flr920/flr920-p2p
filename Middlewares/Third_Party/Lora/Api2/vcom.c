 /******************************************************************************
  * @file    vcom.c
  * @author  MCD Application Team
  * @version V1.1.4
  * @date    08-January-2018
  * @brief   manages virtual com port
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
#include "hw.h"
#include "vcom.h"
#include <stdarg.h>

#include "low_power_manager.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define BUFSIZE 256
#define MAX_PRINT_SIZE 128

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

static struct {
  char buffTx[256];                         /* structure have to be simplified*/
  char buffRx[256];
  int rx_idx_free;
  int rx_idx_toread;
  HW_LockTypeDef Lock;
  __IO HAL_UART_StateTypeDef gState;
  __IO HAL_UART_StateTypeDef RxState;
} uart_context;

/* buffer */
static char buff[BUFSIZE];
/* buffer write index*/
__IO uint16_t iw=0;
/* buffer read index*/
static uint16_t ir=0;
/* Uart Handle */
UART_HandleTypeDef UartHandle;
UART_WakeUpTypeDef Selection;

/* Private function prototypes -----------------------------------------------*/
/**
 * @brief  Takes one character that has been received and save it in uart_context.buffRx
 * @param  received character
 */
static void receive(char rx);

/* Functions Definition ------------------------------------------------------*/

void vcom_Init(uint32_t BaudRate)
{
  /*## Configure the UART peripheral ######################################*/
  if (UARTX == LPUART1)
    __HAL_RCC_LPUART1_CONFIG(RCC_CCIPR_LPUART1SEL_1);

  /* Put the USART peripheral in the Asynchronous mode (UART Mode) */
  UartHandle.Instance        = UARTX;
  UartHandle.Init.BaudRate   = BaudRate;
  UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
  UartHandle.Init.StopBits   = UART_STOPBITS_1;
  UartHandle.Init.Parity     = UART_PARITY_NONE;
  UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
  UartHandle.Init.Mode       = UART_MODE_TX_RX;
  
  if(HAL_UART_Init(&UartHandle) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler(); 
  }

  HAL_NVIC_SetPriority(UARTX_IRQn, 0x1, 0);
  HAL_NVIC_EnableIRQ(UARTX_IRQn);

  if (UARTX == LPUART1)
  {
    /* Configuring the LPUART specific LP feature - the wakeup from STOP */
    Selection.WakeUpEvent = UART_WAKEUP_ON_STARTBIT;
    HAL_UARTEx_EnableClockStopMode(&UartHandle);
    HAL_UARTEx_EnableStopMode( &UartHandle );
    HAL_UARTEx_StopModeWakeUpSourceConfig( &UartHandle, Selection );
    __HAL_UART_ENABLE_IT(&UartHandle, UART_IT_WUF);
  }
}


void vcom_DeInit(void)
{
#if 1
  HAL_UART_DeInit(&UartHandle);
#endif
}

void vcom_IoInit(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct={0};
    /* Enable GPIO TX/RX clock */
  UARTX_TX_GPIO_CLK_ENABLE();
  UARTX_RX_GPIO_CLK_ENABLE();
    /* UART TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = UARTX_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
  GPIO_InitStruct.Alternate = UARTX_TX_AF;

  HAL_GPIO_Init(UARTX_TX_GPIO_PORT, &GPIO_InitStruct);

  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin = UARTX_RX_PIN;
  GPIO_InitStruct.Alternate = UARTX_RX_AF;

  HAL_GPIO_Init(UARTX_RX_GPIO_PORT, &GPIO_InitStruct);
}

void vcom_IoDeInit(void)
{
  GPIO_InitTypeDef GPIO_InitStructure={0};
  
  UARTX_TX_GPIO_CLK_ENABLE();
  UARTX_RX_GPIO_CLK_ENABLE();

  GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStructure.Pull = GPIO_NOPULL;
  
  GPIO_InitStructure.Pin =  UARTX_TX_PIN ;
  HAL_GPIO_Init(  UARTX_TX_GPIO_PORT, &GPIO_InitStructure );
  
  GPIO_InitStructure.Pin =  UARTX_RX_PIN ;
  HAL_GPIO_Init(  UARTX_RX_GPIO_PORT, &GPIO_InitStructure ); 
}

void vcom_ReceiveInit(void)
{
  /*Enable RX Not Empty Interrupt*/
  SET_BIT(UartHandle.Instance->CR1, USART_CR1_RXNEIE);
	/* WakeUp from stop mode on start bit detection*/
  //MODIFY_REG(UartHandle.Instance->CR3, USART_CR3_WUS, USART_CR3_WUS_1);
	
  SET_BIT(UartHandle.Instance->CR3, USART_CR3_WUFIE);
  /* Enable the UART Parity Error Interrupt */
  SET_BIT(UartHandle.Instance->CR1, USART_CR1_PEIE);
  /* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
  SET_BIT(UartHandle.Instance->CR3, USART_CR3_EIE);
}


void vcom_Send2(uint8_t *pData, uint32_t txSize)
{
    HAL_UART_Transmit(&UartHandle,(uint8_t *) pData, txSize, 300);
}



void vcom_Send( const char *format, ... )
{
#if 1

    va_list args;
    va_start(args, format);
    uint8_t len;
    char tempBuff[1024]; // ewbm_swhong : tx buffer extended size up 1024

    len = vsprintf(&tempBuff[0], format, args);
    va_end(args);

    HAL_UART_Transmit(&UartHandle,(uint8_t *) tempBuff, len, 300);
    //HAL_UART_Transmit_DMA(&UartHandle, (uint8_t *)tempBuff, len);

#else
  va_list args;
  va_start(args, format);
  uint8_t len;
  uint8_t lenTop;
  char tempBuff[128];

  BACKUP_PRIMASK();
  DISABLE_IRQ();
  
  /*convert into string at buff[0] of length iw*/
  len = vsprintf(&tempBuff[0], format, args);
  
  if (iw+len<BUFSIZE)
  {
    memcpy( &buff[iw], &tempBuff[0], len);
    iw+=len;
  }
  else
  {
    lenTop=BUFSIZE-iw;
    memcpy( &buff[iw], &tempBuff[0], lenTop);
    len-=lenTop;
    memcpy( &buff[0], &tempBuff[lenTop], len);
    iw = len;
  }
  RESTORE_PRIMASK();
  
  HAL_NVIC_SetPendingIRQ(UARTX_IRQn);
    
  va_end(args);
#endif
}

/* modifes only ir*/
void vcom_Print( void)
{
  char* CurChar;
  while( ( (iw+BUFSIZE-ir)%BUFSIZE) >0 )
  {
    BACKUP_PRIMASK();
    DISABLE_IRQ();
    
    CurChar = &buff[ir];
    ir= (ir+1) %BUFSIZE;
    
    RESTORE_PRIMASK();
    
    HAL_UART_Transmit(&UartHandle,(uint8_t *) CurChar, 1, 300);    
  }
  HAL_NVIC_ClearPendingIRQ(UARTX_IRQn);
}

void vcom_Send_Lp( char *format, ... )
{
  va_list args;
  va_start(args, format);
  uint8_t len;
  uint8_t lenTop;
  char tempBuff[128];
  
  BACKUP_PRIMASK();
  DISABLE_IRQ();
  
  /*convert into string at buff[0] of length iw*/
  len = vsprintf(&tempBuff[0], format, args);
  
  if (iw+len<BUFSIZE)
  {
    memcpy( &buff[iw], &tempBuff[0], len);
    iw+=len;
  }
  else
  {
    lenTop=BUFSIZE-iw;
    memcpy( &buff[iw], &tempBuff[0], lenTop);
    len-=lenTop;
    memcpy( &buff[0], &tempBuff[lenTop], len);
    iw = len;
  }
  RESTORE_PRIMASK();  
  
  va_end(args);
}

FlagStatus IsNewCharReceived(void)
{
  FlagStatus status;
  
//  BACKUP_PRIMASK();
//  DISABLE_IRQ();
  
  status = ((uart_context.rx_idx_toread == uart_context.rx_idx_free) ? RESET : SET);
  
//  RESTORE_PRIMASK();
  return status;
}

uint8_t GetNewChar(void)
{
  uint8_t NewChar;

  BACKUP_PRIMASK();
  DISABLE_IRQ();

  NewChar = uart_context.buffRx[uart_context.rx_idx_toread];
  uart_context.rx_idx_toread = (uart_context.rx_idx_toread + 1) % sizeof(uart_context.buffRx);

  RESTORE_PRIMASK();
  return NewChar;
}

void vcom_IRQHandler(void)
{
  UART_HandleTypeDef *huart = &UartHandle;

  uint32_t isrflags   = READ_REG(huart->Instance->ISR);
  uint32_t cr1its     = READ_REG(huart->Instance->CR1);
  uint32_t cr3its = READ_REG(huart->Instance->CR3);;
  uint32_t errorflags;
//  uint16_t  uhMask = huart->Mask;
//  uint16_t  uhdata;
  int rx_ready = 0;



    /* UART wakeup from Stop mode interrupt occurred ---------------------------*/
    if(((isrflags & USART_ISR_WUF) != RESET) && ((cr3its & USART_CR3_WUFIE) != RESET))
    {
      __HAL_UART_CLEAR_IT(huart, UART_CLEAR_WUF);

       /* forbid stop mode */
		// wjkim-delete	 LPM_SetStopMode(LPM_UART_RX_Id , LPM_Disable );

      /* Enable the UART Data Register not empty Interrupts */
      SET_BIT(huart->Instance->CR1, USART_CR1_RXNEIE);

      /* Set the UART state ready to be able to start again the process */
      huart->gState  = HAL_UART_STATE_READY;
      huart->RxState = HAL_UART_STATE_READY;

    }


	/* UART in mode Receiver ---------------------------------------------------*/
  if(((isrflags & USART_ISR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET))
  {
		/* Check that a Rx process is ongoing */
		if(huart->RxState == HAL_UART_STATE_BUSY_RX)
		{
		        /*RXNE flag is auto cleared by reading the data*/
                        *huart->pRxBuffPtr++ = (uint8_t)READ_REG(huart->Instance->RDR);

                        /* allow stop mode*/
                        // wjkim-delete LPM_SetStopMode(LPM_UART_RX_Id , LPM_Enable );

			if(--huart->RxXferCount == 0U)
                        {
				CLEAR_BIT(huart->Instance->CR1, (USART_CR1_RXNEIE | USART_CR1_PEIE));
				CLEAR_BIT(huart->Instance->CR3, USART_CR3_EIE);
				huart->RxState = HAL_UART_STATE_READY;
				rx_ready = 1;  /* not used RxTC callback*/
			}
    }
    else
		{
       /* Clear RXNE interrupt flag */
       //__HAL_UART_SEND_REQ(huart, UART_RXDATA_FLUSH_REQUEST);
       //return;
      
      /* allow stop mode*/
      // wjkim-delete LPM_SetStopMode(LPM_UART_RX_Id, LPM_Enable);
			receive((uint8_t)(huart->Instance->RDR));
			return;
		}
  }

	  /* If error occurs */
     errorflags = (isrflags & (uint32_t)(USART_ISR_PE | USART_ISR_FE | USART_ISR_ORE | USART_ISR_NE));
     if (errorflags != RESET)
     {
	   /* Error on receiving */
        __HAL_UART_CLEAR_IT(huart, UART_CLEAR_PEF);
        __HAL_UART_CLEAR_IT(huart, UART_CLEAR_FEF);
        __HAL_UART_CLEAR_IT(huart, UART_CLEAR_OREF);
        __HAL_UART_CLEAR_IT(huart, UART_CLEAR_NEF);
//	   *((huart->pRxBuffPtr)-1) = 0x01;           /*we skip the overrun case*/
	   rx_ready = 1;
	 }

	if(rx_ready)
	{
	  /*character in the ring buffer*/
	  receive(*((huart->pRxBuffPtr)-1));
	}
}

/******************************************************************************
  * @brief Store in ring buffer the received character
  * @param none
  * @retval none
******************************************************************************/
static void receive(char rx)
{

  int next_free;

  /** no need to clear the RXNE flag because it is auto cleared by reading the data*/
  uart_context.buffRx[uart_context.rx_idx_free] = rx;
  next_free = (uart_context.rx_idx_free + 1) % sizeof(uart_context.buffRx);
  if (next_free != uart_context.rx_idx_toread)
  {
    /* this is ok to read as there is no buffer overflow in input */
    uart_context.rx_idx_free = next_free;
  }


//  else
//  {
//    /* force the end of a command in case of overflow so that we can process it */
//    uart_context.buffRx[uart_context.rx_idx_free] = '\r';
//    PRINTF("uart_context.buffRx buffer overflow %d\r\n");
//  }
}

void vcom_Dma_IRQHandler( void )
{
}


/* USART1 and USART2 init function */

void USART1_IoInit(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    /* Peripheral clock enable */
    __HAL_RCC_USART1_CLK_ENABLE();

    /***************************************************************/
    /*      GPIO Configuration   for UART1 and for UART2           */
    /*           PA2     ------> USART2_TX                         */
    /*           PA3     ------> USART2_RX                         */
    /*           PA9     ------> USART1_TX                         */
    /*           PA10    ------> USART1_RX                         */
    /***************************************************************/

    GPIO_InitStruct.Pin = GPIO_PIN_9;

    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;//GPIO_SPEED_FREQ_MEDIUM;

    GPIO_InitStruct.Alternate = GPIO_AF4_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_10;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}


/**
  * @brief UART MSP Initialization 
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  *           - NVIC configuration for UART interrupt request enable
  * @param huart: UART handle pointer
  * @retval None
  */
void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
    if (huart->Instance == USART5)
    {       
        /* Enable USART1 clock */
        UARTX_CLK_ENABLE(); 

        /*##-2- Configure peripheral GPIO ##########################################*/  
        vcom_IoInit( );
    } 
    else if( huart->Instance == USART1)
    {
        static DMA_HandleTypeDef hdma_tx;

        /* Enable GPIO TX/RX clock */
        __GPIOA_CLK_ENABLE();
        __GPIOA_CLK_ENABLE();

        /* Enable USARTx clock */
        __USART1_CLK_ENABLE();

        /* select USARTx clock source*/
        RCC_PeriphCLKInitTypeDef  PeriphClkInit = {0};
        PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
        PeriphClkInit.Lpuart1ClockSelection = RCC_USART1CLKSOURCE_HSI;
        HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);
       
        /* Enable DMA clock */
        __HAL_RCC_DMA1_CLK_ENABLE();
        
         USART1_IoInit();
        
        /* Configure the DMA handler for Transmission process */
        hdma_tx.Instance                 = DMA1_Channel4;
        hdma_tx.Init.Request             = DMA_REQUEST_3;
        hdma_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
        hdma_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
        hdma_tx.Init.MemInc              = DMA_MINC_ENABLE;
        hdma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
        hdma_tx.Init.Mode                = DMA_NORMAL;
        hdma_tx.Init.Priority            = DMA_PRIORITY_LOW;
        
       if(HAL_DMA_Init(&hdma_tx) != HAL_OK)
       {
            Error_Handler();
       }

        /* Associate the initialized DMA handle to the UART handle */
        __HAL_LINKDMA(huart, hdmatx, hdma_tx);

        /*##-4- Configure the NVIC for DMA #########################################*/
        /* NVIC configuration for DMA transfer complete interrupt (USART1_TX) */
        HAL_NVIC_SetPriority(DMA1_Channel4_5_6_7_IRQn, 0, 1);
        HAL_NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);

        /* NVIC for USART, to catch the TX complete */
        HAL_NVIC_SetPriority(USART1_IRQn, 0, 1);
        HAL_NVIC_EnableIRQ(USART1_IRQn);
    }
}

/**
  * @brief UART MSP DeInit
  * @param huart: uart handle
  * @retval None
  */
void HAL_UART_MspDeInit(UART_HandleTypeDef *huart)
{
//  vcom_IoDeInit( );
}

/*-----------------------------------------------------------------------------*/
#define TEMPBUFSIZE 256
#define DBG_TRACE_MSG_QUEUE_SIZE 256


__IO ITStatus TracePeripheralReady = SET;

#define ELEMENT_SIZE_LEN 2
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

extern void vcom_Trace(uint8_t *p_data, uint16_t size);

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief  Trace buffer Transfer completed callback
 * @param  none
 * @note   Indicate the end of the transmission of a  trace buffer. If queue
 *         contains new trace data to transmit, start a new transmission.
 * @retval None
 */




/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
