/*******************************************************************************
 * @file    command.c
 * @author  MCD Application Team
 * @version V1.2.0
 * @date    10-July-2018
 * @brief   main command driver dedicated to command AT
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V.
 * All rights reserved.</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include "at_ewbm.h"
#include "hw.h"
#include "app.h"



/* comment the following to have help message */
#define NO_HELP
/* #define NO_KEY_ADDR_EUI */

/* Private typedef -----------------------------------------------------------*/
/**
 * @brief  Structure defining an AT Command
 */
struct ATCommand_s {
  const char *string;                       /*< command string, after the "AT" */
  const int size_string;                    /*< size of the command string, not including the final \0 */
  ATEerror_t (*get)(const char *param);     /*< =? after the string to get the current value*/
  ATEerror_t (*set)(const char *param);     /*< = (but not =?\0) after the string to set a value */
  ATEerror_t (*run)(const char *param);     /*< \0 after the string - run the command */
#if !defined(NO_HELP)
  const char *help_string;                  /*< to be printed when ? after the string */
#endif
};

/* Private define ------------------------------------------------------------*/
#define CMD_SIZE 1024

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/**
 * @brief  Array corresponding to the description of each possible AT Error
 */
static const char *const ATError_description[] =
{
  "[OK]\r\n",                   /* AT_OK */
  "[FAIL]\r\n",                 /* AT_ERROR */
  "[PARAM ERROR]\r\n",          /* AT_PARAM_ERROR */
  "[BUSY]\r\n",                /* AT_BUSY_ERROR */
  "[OVERFLOW]\r\n",             /* AT_TEST_PARAM_OVERFLOW */
  "[NOTJOINED]\r\n",            /* AT_NO_NET_JOINED */
  "[RX ERROR]\r\n",             /* AT_RX_ERROR */
  "[MAX ERROR]\r\n",            /* AT_MAX */
};

/**
 * @brief  Array of all supported AT Commands
 */
static const struct ATCommand_s ATCommand[] =
{
    {
        .string = AT_DEVEUI,
        .size_string = sizeof(AT_DEVEUI) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_get_dev_eui,
        .set = at_set_dev_eui,
        .run = at_get_dev_eui,
    },
    {
        .string = AT_AESPWD,
        .size_string = sizeof(AT_AESPWD) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_get_aes_pwd,
        .set = at_set_aes_pwd,
        .run = at_get_aes_pwd,
    },
    {
        .string = AT_DBGL,
        .size_string = sizeof(AT_DBGL) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_get_p2p_dbgl,
        .set = at_set_p2p_dbgl,
        .run = at_get_p2p_dbgl,
    },
    {
        .string = AT_CHANNEL,
        .size_string = sizeof(AT_CHANNEL) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_get_p2p_channel,
        .set = at_set_p2p_channel,
        .run = at_get_p2p_channel,
    },
    {
            .string = AT_RATESET,
            .size_string = sizeof(AT_RATESET) - 1,
#ifndef NO_HELP
            .help_string = "\r\n",
#endif
            .get = at_get_p2p_rateset,
            .set = at_set_p2p_rateset,
            .run = at_get_p2p_rateset,
    },
    {
        .string = AT_PAN_ID,
        .size_string = sizeof(AT_PAN_ID) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_get_p2p_pan_id,
        .set = at_set_p2p_pan_id,
        .run = at_get_p2p_pan_id,
    },
    {
            .string = AT_NODE_ID_SET,
            .size_string = sizeof(AT_NODE_ID_SET) - 1,
#ifndef NO_HELP
            .help_string = "\r\n",
#endif
            .get = at_return_error,
            .set = at_set_p2p_node_id,
            .run = at_return_error,
    },
    {
        .string = AT_NODE_ID,
        .size_string = sizeof(AT_NODE_ID) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_get_p2p_node_id,
        .set = at_return_error,
        .run = at_get_p2p_node_id,
    },
    {
        .string = AT_NULLMODEM,
        .size_string = sizeof(AT_NULLMODEM) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_get_p2p_null_modem,
        .set = at_set_p2p_null_modem,
        .run = at_get_p2p_null_modem,
    },
    {
        .string = AT_ENERGY_SCAN_ALL,
        .size_string = sizeof(AT_ENERGY_SCAN_ALL) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_return_error,
        .set = at_set_p2p_energy_scan_all,
        .run = at_return_error,
    },
    {
        .string = AT_ENERGY_SCAN,
        .size_string = sizeof(AT_ENERGY_SCAN) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_return_error,
        .set = at_set_p2p_energy_scan,
        .run = at_return_error,
    },
    {
        .string = AT_HELP,
        .size_string = sizeof(AT_HELP) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_set_p2p_help,
        .set = at_set_p2p_help,
        .run = at_set_p2p_help,
    },
    {
        .string = AT_RESET,
        .size_string = sizeof(AT_RESET) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_set_p2p_reset,
        .set = at_set_p2p_reset,
        .run = at_set_p2p_reset,
    },
    {
        .string = AT_FSET,
        .size_string = sizeof(AT_FSET) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_set_p2p_fset,
        .set = at_set_p2p_fset,
        .run = at_set_p2p_fset,
    },
    {
        .string = AT_RFPOWER,
        .size_string = sizeof(AT_RFPOWER) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_get_p2p_rfpower,
        .set = at_set_p2p_rfpower,
        .run = at_get_p2p_rfpower,
    },
    {
        .string = AT_FWVER,
        .size_string = sizeof(AT_FWVER) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_get_p2p_fwver,
        .set = at_get_p2p_fwver,
        .run = at_get_p2p_fwver,
    },
    {
        .string = AT_AES128,
        .size_string = sizeof(AT_AES128) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_get_p2p_aes128,
        .set = at_set_p2p_aes128,
        .run = at_get_p2p_aes128,
    },
    {
        .string = AT_PING,
        .size_string = sizeof(AT_PING) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_return_error,
        .set = at_set_p2p_ping,
        .run = at_return_error,
    },
    {
        .string = AT_BAUDRATE,
        .size_string = sizeof(AT_BAUDRATE) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_get_p2p_baudrate,
        .set = at_set_p2p_baudrate,
        .run = at_get_p2p_baudrate,
    },
    {
        .string = AT_SEND,
        .size_string = sizeof(AT_SEND) - 1,
#ifndef NO_HELP
        .help_string = "\r\n",
#endif
        .get = at_return_error,
        .set = at_set_p2p_send,
        .run = at_return_error,
    },

    {
        .string = AT_BDLOAD,
        .size_string = sizeof(AT_BDLOAD) - 1,
#ifndef NO_HELP
        .help_string = "AT"AT_BDLOAD ": Boot download \r\n",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_boot_download,
    },

    {
        .string = AT_HOPLEVEL,
        .size_string = sizeof(AT_HOPLEVEL) - 1,
#ifndef NO_HELP
        .help_string = "AT"AT_HOPLEVEL ": HOP LEVEL \r\n",
#endif
        .get = at_get_hop_level,
        .set = at_set_hop_level,
        .run = at_get_hop_level,
    },
    {
        .string = AT_HOPDELAY,
        .size_string = sizeof(AT_HOPDELAY) - 1,
#ifndef NO_HELP
        .help_string = "AT"AT_HOPDELAY ": HOP DELAY \r\n",
#endif
        .get = at_get_hop_delay,
        .set = at_set_hop_delay,
        .run = at_get_hop_delay,
    },
    {
        .string = AT_SLEEP,
        .size_string = sizeof(AT_SLEEP) - 1,
#ifndef NO_HELP
        .help_string = "AT"AT_SLEEP ": SLEEP \r\n",
#endif
        .get = at_return_error,
        .set = at_set_sleep,
        .run = at_return_error,
    },
    {
        .string = AT_RXDATA,
        .size_string = sizeof(AT_RXDATA) - 1,
#ifndef NO_HELP
        .help_string = "AT"AT_RXDATA ": RXDATA \r\n",
#endif
        .get = at_get_rxdata,
        .set = at_return_error,
        .run = at_get_rxdata,
    },
    {
        .string = AT_RECVFMT,
        .size_string = sizeof(AT_RECVFMT) - 1,
#ifndef NO_HELP
        .help_string = "AT"AT_RECVFMT ": AT_RECVFMT \r\n",
#endif
        .get = at_get_recv_fmt,
        .set = at_set_recv_fmt,
        .run = at_get_recv_fmt,
    },
    {
        .string = AT_TICK,
        .size_string = sizeof(AT_TICK) - 1,
#ifndef NO_HELP
        .help_string = "AT"AT_TICK ": AT_TICK \r\n",
#endif
        .get = at_get_tick,
        .set = at_get_tick,
        .run = at_get_tick,
    },


};


/* Private function prototypes -----------------------------------------------*/

/**
 * @brief  Print a string corresponding to an ATEerror_t
 * @param  The AT error code
 * @retval None
 */
static void com_error(ATEerror_t error_type);

/**
 * @brief  Parse a command and process it
 * @param  The command
 * @retval None
 */
static void parse_cmd(char *cmd_data, uint32_t cmd_size);

/* Exported functions ---------------------------------------------------------*/

void CMD_Init(uint32_t BaudRate)
{
  vcom_Init(BaudRate);
  vcom_ReceiveInit();
}


#if 1
void CMD_Process(void)
{
    static char command[CMD_SIZE];
    static unsigned i = 0;
    static uint32_t last_tick = 0;
 
    while (IsNewCharReceived() == SET) {
        last_tick = HAL_GetTick();
        command[i++] = GetNewChar();

        if (i == (CMD_SIZE - 1)) {
            i = 0;
            com_error(AT_TEST_PARAM_OVERFLOW);
        }
    }
    
    if(i > 0 && (HAL_GetTick() - last_tick) > 100) {
        command[i+1] = '\0';
        parse_cmd(command, i);
        i = 0;
    }
               

}


#else
void CMD_Process(void)
{
    static char command[CMD_SIZE];
    static unsigned i = 0;

    /* Process all commands */
    while (IsNewCharReceived() == SET)
    {
        command[i] = GetNewChar();

        if (command[i] == AT_ERROR_RX_CHAR)
        {
            i = 0;
            com_error(AT_RX_ERROR);
            break;
        }
        else if ((command[i] == '\r') || (command[i] == '\n')) {

            if (i != 0) {
                command[i] = '\0';
                parse_cmd(command);
                i = 0;
            }
        }
        else if (command[i] == ']') {
            if (i != 0) {
                command[i+1] = '\0';
                parse_cmd(command);
                i = 0;
            }
        }
        else if (i == (CMD_SIZE - 1)) {
            i = 0;
            com_error(AT_TEST_PARAM_OVERFLOW);
        }
        else {
            i++;
        }
    }
}
#endif

/* Private functions ---------------------------------------------------------*/

static void com_error(ATEerror_t error_type)
{
  if (error_type > AT_MAX)
  {
    error_type = AT_MAX;
  }
  AT_PRINTF(ATError_description[error_type]);
}



char *RemoveGoro(char *cmd_data, bool *garo, uint32_t size)
{
    char *ret = cmd_data;
    int nLen = size;
    
    sAppInfoType *pAppInfo = GetATCommandInfo();

    *garo = false;
    
    if(nLen >=2 && (cmd_data[nLen-2] == '\r' && cmd_data[nLen-1]  == '\n')) {
        cmd_data[nLen-2] = NULL;
    }
    else if(nLen >=1 && (cmd_data[nLen-1] == '\r' || cmd_data[nLen-1]  == '\n')) {
        cmd_data[nLen-1] = NULL;
    }


    if(size >=9 && strncmp(cmd_data, "AT+BDLOAD", 9) == 0) {
        return cmd_data;
    }

    if(size >=9 && strncmp(cmd_data, "AT+DEVEUI", 9) == 0) {
        return cmd_data;
    }

    if(cmd_data[0] == '[') {
        if(cmd_data[1] == 'A' && cmd_data[2] == 'T') {
            for(int i=3; i<nLen; i++) {
                if(cmd_data[i] == ']') {
                    cmd_data[i] = NULL;
                    *garo = true;
                    break;
                }
            }
        }
    }

    if(pAppInfo != NULL && pAppInfo->u8NullModem) {
        if(*garo == false) {
            if(cmd_data[0] == '<') {
                for(int i=1; i<nLen; i++) {
                    if(cmd_data[i] == '>') {
                        return cmd_data;
                    }
                }
            }
            return NULL;
        }
    }
    

    if(*garo == false) {
        return NULL;
    }
      
        
    ret = &cmd_data[1];
   
    return ret;
}





static void parse_cmd(char *cmd_data, uint32_t cmd_size)
{
    int i;
    bool isGaro = false;
    ATEerror_t status = AT_OK;
    const struct ATCommand_s *Current_ATCommand;

    char *cmd = RemoveGoro(cmd_data, &isGaro, cmd_size);

    if(cmd == NULL) {
        com_error(AT_ERROR);
        return;
    }



    if (cmd[0] == 'A' && cmd[1] == 'T' && cmd[2] == '\0')
    {
        /* status = AT_OK; */
    }
    else if (cmd[0] == 'A' && cmd[1] == 'T' && cmd[2] == '?')
    {
        status = AT_OK;
    }
    else {
        /* point to the start of the command, excluding AT */
        status = AT_PARAM_ERROR;

        if (cmd[0] == 'A' && cmd[1] == 'T') {
            cmd += 2;

            for (i = 0; i < (sizeof(ATCommand) / sizeof(struct ATCommand_s)); i++) {

                if (strncmp(cmd, ATCommand[i].string, ATCommand[i].size_string) == 0) {

                    Current_ATCommand = &(ATCommand[i]);
                    cmd += Current_ATCommand->size_string;

                    /* parse after the command */
                    switch (cmd[0]) {
                        case '\0':    /* nothing after the command */
                            status = Current_ATCommand->run(cmd);
                            break;
                        
                        case '=':
                        case ' ':
                            if ((cmd[1] == '?') && (cmd[2] == '\0'))
                            {
                                status = Current_ATCommand->get(cmd + 1);
                            }
                            else
                            {
                                status = Current_ATCommand->set(cmd + 1);
                            }
                            break;
                            
                        case '?':
#ifndef NO_HELP
                            AT_PRINTF(Current_ATCommand->help_string);
#endif
                            status = AT_OK;
                            break;
                            
                        default:
                            /* not recognized */
                            break;
                    }

                    /* we end the loop as the command was found */
                    break;
                }

            }
        }
        else {
            status = at_set_p2p_send_all(cmd, isGaro, cmd_size);
        }


    }

    com_error(status);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
