/*******************************************************************************
 * @file    at_ewbm.h
 * @author  MCD Application Team
 * @version V1.1.4
 * @date    08-January-2018
 * @brief   Header for driver at.c module
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V.
 * All rights reserved.</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AT_EWBM_H__
#define __AT_EWBM_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/* Exported types ------------------------------------------------------------*/
/*
 * AT Command Id errors. Note that they are in sync with ATError_description static array
 * in command.c
 */
typedef enum eATEerror
{
  AT_OK = 0,
  AT_ERROR,
  AT_PARAM_ERROR,
  AT_BUSY_ERROR,
  AT_TEST_PARAM_OVERFLOW,
  AT_NO_NET_JOINED,
  AT_RX_ERROR,
  AT_MAX,
} ATEerror_t;

/* Exported constants --------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* AT printf */
#define AT_PRINTF(...)     vcom_Send(__VA_ARGS__)


extern void SetATCommandInfo(void *pInfo);
extern void* GetATCommandInfo(void);



#define AT_DEVEUI           "+DEVEUI"
#define AT_AESPWD           "+AESPWD"
#define AT_DBGL             "+DBGL"
#define AT_CHANNEL          "+CHANNEL"
#define AT_PAN_ID           "+PAN_ID"
#define AT_NODE_ID          "+NODE_ID"
#define AT_NODE_ID_SET      "+NODE_ID_SET"
#define AT_NULLMODEM        "+NULLMODEM"

#define AT_ENERGY_SCAN_ALL  "+ENERGY_SCAN_ALL"
#define AT_ENERGY_SCAN      "+ENERGY_SCAN"

#define AT_HELP             "+HELP"
#define AT_RESET            "+RESET"
#define AT_FSET             "+FSET"

#define AT_RFPOWER          "+RFPOWER"
#define AT_RATESET          "+RATESET"

#define AT_FWVER            "+FWVER"
#define AT_AES128           "+AES128"

#define AT_PING             "+PING"
#define AT_BAUDRATE         "+BAUDRATE"

#define AT_SEND             "+SEND"
#define AT_RECV             "+RECV"

#define AT_BDLOAD           "+BDLOAD"


#define AT_HOPLEVEL         "+HOPLEVEL"
#define AT_HOPDELAY         "+HOPDELAY"

#define AT_SLEEP            "+SLEEP"
#define AT_RXDATA           "+RXDATA"
#define AT_RECVFMT          "+RECVFMT"
#define AT_TICK             "+TICK"









void reset_reason(uint32_t reason);


/**
 * @brief  Return AT_OK in all cases
 * @param  Param string of the AT command - unused
 * @retval AT_OK
 */
ATEerror_t at_return_ok(const char *param);

/**
 * @brief  Return AT_ERROR in all cases
 * @param  Param string of the AT command - unused
 * @retval AT_ERROR
 */
ATEerror_t at_return_error(const char *param);

ATEerror_t at_get_dev_eui(const char *param);
ATEerror_t at_set_dev_eui(const char *param);

ATEerror_t at_get_aes_pwd(const char *param);
ATEerror_t at_set_aes_pwd(const char *param);

ATEerror_t at_get_p2p_dbgl(const char *param);
ATEerror_t at_set_p2p_dbgl(const char *param);


ATEerror_t at_get_p2p_channel(const char *param);
ATEerror_t at_set_p2p_channel(const char *param);

ATEerror_t at_get_p2p_rateset(const char *param);
ATEerror_t at_set_p2p_rateset(const char *param);

ATEerror_t at_get_p2p_pan_id(const char *param);
ATEerror_t at_set_p2p_pan_id(const char *param);

ATEerror_t at_get_p2p_node_id(const char *param);
ATEerror_t at_set_p2p_node_id(const char *param);


ATEerror_t at_get_p2p_null_modem(const char *param);
ATEerror_t at_set_p2p_null_modem(const char *param);

ATEerror_t at_set_p2p_energy_scan(const char *param);
ATEerror_t at_set_p2p_energy_scan_all(const char *param);


ATEerror_t at_set_p2p_help(const char *param);
ATEerror_t at_set_p2p_reset(const char *param);
ATEerror_t at_set_p2p_fset(const char *param);

ATEerror_t at_get_p2p_rfpower(const char *param);
ATEerror_t at_set_p2p_rfpower(const char *param);

ATEerror_t at_get_p2p_fwver(const char *param);

ATEerror_t at_get_p2p_aes128(const char *param);
ATEerror_t at_set_p2p_aes128(const char *param);


ATEerror_t at_set_p2p_ping(const char *param);

ATEerror_t at_get_p2p_baudrate(const char *param);
ATEerror_t at_set_p2p_baudrate(const char *param);


ATEerror_t at_set_p2p_send(const char *param);
ATEerror_t at_set_p2p_send_all(const char *param, bool isGaro, uint32_t size);


ATEerror_t at_boot_download(const char *param);


ATEerror_t at_get_hop_level(const char *param);
ATEerror_t at_set_hop_level(const char *param);

ATEerror_t at_get_hop_delay(const char *param);
ATEerror_t at_set_hop_delay(const char *param);

ATEerror_t at_set_sleep(const char *param);
ATEerror_t at_get_rxdata(const char *param);

ATEerror_t at_get_recv_fmt(const char *param);
ATEerror_t at_set_recv_fmt(const char *param);

ATEerror_t at_get_tick(const char *param);



/**
 * @brief  Trig a reset of the MCU
 * @param  Param string of the AT command - unused
 * @retval AT_OK
 */




#ifdef __cplusplus
}
#endif

#endif /* __AT_EWBM_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
