/*
** FLYVO.CO.KR
** wjkim@flyvo.co.kr
*/



#define MAIN_C_


/* Includes ------------------------------------------------------------------*/
#include "hw.h"
#include "low_power_manager.h"
#include "ewbm_lora_storage.h"
#include "timer.h"
#include "main.h"
#include "Radio.h"
#include "version.h"
#include "app.h"



#define PRINTF(...)             vcom_Send(__VA_ARGS__)

extern void                     CMD_Init(uint32_t BaudRate);
extern void                     CMD_Process(void);



static bool                     isReadySleep = false;
static int                      sleep_second_time = 10;


void SetSleepEnter(uint32_t nSecond)
{
    sleep_second_time = nSecond;
    isReadySleep = true;
}



/* Sleep wakeup timer callback */
void alarmb_wakeup_cb(RTC_HandleTypeDef *hrtc)
{
    LPM_ExitStopMode();

    /* Stop Alarm B
    */
    HW_RTC_StopAlarmB();

    /* ADD-CODE */

}





/*==========================================================================

FUNCTION MAIN

DESCRIPTION
Main application entry point.

DEPENDENCIES

RETURN VALUE
None

SIDE EFFECTS
None

===========================================================================*/
int main
( 
void 
)
{ 
    sAppInfoType *pAppInfo;

    /* STM32 HAL library initialization
    */
    HAL_Init( );

    /* Configure the system clock
    */ 
    SystemClock_Config( );

    /* Configure the debug mode
    */
    DBG_Init( );

    /* storage init
    */
    ewbm_lora_storage_init();

    /* Configure the hardware
    */
    HW_Init( );


    pAppInfo = App_Initialize();
    
    /* configure the command mode
    */
   
    CMD_Init(pAppInfo->u32Baudrate);

    
    /*Disable standby mode*/
    LPM_SetOffMode(LPM_APPLI_Id, LPM_Disable);

    
    PRINTF("[Flyvo.co.kr]\r\n");
    PRINTF("[LoRa P2P Solution]\r\n");
    PRINTF("[F/W VERSION  %s]\r\n", P2P_VERSION_STRING);

    PRINTF("[CHANNEL %d, NODEID %d, HOP %d]\r\n", pAppInfo->u8Channel, pAppInfo->u16NodeID, pAppInfo->u8HopLevel);
    PRINTF("[NULL MODEM %d, RFPOWER %d]\r\n", pAppInfo->u8NullModem, pAppInfo->u8Power);

    App_Start();

    while( 1 ) { 
        /* LoRa Protocol is executed by going through an infinite loop.
        */
        CMD_Process();  

        /* Battery Levlel Checking */
        #ifdef BATTERY_CHECK
        HW_GetBatteryLevel();  // wjkim-battery
        #endif
			
        /*DO NOT Delet!! Checking if set sleep flag
        */
        if(isReadySleep) {
            
            isReadySleep = false;
            PRINTF("[SLEEP START TICK=%d]\r\n", HAL_GetTick());
            set_sleep_wakeup_timer(sleep_second_time);
            
            PRINTF("[SLEEP END TICK=%d]\r\n", HAL_GetTick());
            App_SleepWakeup();
        }

    }
}





