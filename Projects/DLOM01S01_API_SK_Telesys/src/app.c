/*
** FLYVO.CO.KR
** wjkim@flyvo.co.kr
*/

// AES, BAUDRATE,  factory reset
// XCOM에서 데이터 받았는디 Display ,안함, "\r\n" 이 없어서


/* Includes ------------------------------------------------------------------*/
#include "hw.h"
#include "low_power_manager.h"
#include "ewbm_lora_storage.h"
#include "timer.h"
#include "app.h"
#include "version.h"
#include "lora.h"
#include "at_ewbm.h"
#include "tiny_sscanf.h"





#define EUI_ARR_SIZE                        8
#define KEY_ARR_SIZE                        16




#define MAX_PING_RETRY                      3
#define MAX_SEND_RETRY                      3
#define MAX_HOP_RETRY                       2

#define NODE_TICK_TIME_INT                  250
#define NODE_FAST_TIME_INT                  5
#define LOW_TIMER_CNT_RESET                 5

#define IS_NODE_TIMEOUT(tick, interval)     ((tick*NODE_TICK_TIME_INT) >= interval)



#define BUFFER_SIZE                         255          // Define the payload size here


#define PRINTH(d,s)                         vcom_Send2(d,s)
#define PRINTF(...)                         vcom_Send(__VA_ARGS__)
#define DPRINTF(...)                        if(NodeAppInfo.u8Dbgl) {         \
                                                vcom_Send(__VA_ARGS__);     \
                                            }





typedef enum {

    NODE_NONE_S,
    NODE_ENTER_S,

    NODE_IDLE_INIT_S,
    NODE_IDLING_S,

    NODE_SEND_REQ_S,
    NODE_SEND_RSP_S,

    NODE_HOP_REQ_S,
    NODE_HOP_RSP_S,

    NODE_PING_REQ_S,
    NODE_PING_RSP_S,
    NODE_PING_NXT_S,
    
    NODE_PONG_REQ_S,
    NODE_PONG_RSP_S,

    NODE_BAUDRATE_S,
    NODE_RESET_S,
    NODE_FSET_S,
    NODE_SLEEP_S,
    NODE_SLEEP_WAKE_S,

    NODE_EXIT_S,
    
}eNodeSateType;


typedef enum {
    TX_PKT_NONE,
    TX_PKT_PING,
    TX_PKT_PONG,
}eTxPaketType;


typedef struct {
    int         tickCount;
    uint32_t    sendTime;
    int         index;
    int         nRetry;
   
    uint16_t    destNode;
    uint8_t     len;
    uint32_t    interval;
    int         count;
    char        data[255];
}sNodePingType;


typedef struct {
    int         tickCount;
    uint16_t    destNode;
    int         nRetry;
    uint8_t     isNullModem;
    bool        isGaro;
    uint32_t    uParam[5];
    char        data[255];
}sNodeRunType;


static RadioEvents_t                RadioEvents;
static sNodeEventType               NodeEvent;
static sAppInfoType                 NodeAppInfo;
static TimerEvent_t                 NodeTickTimer;

static eNodeSateType                CurNodeState;
static eNodeSateType                NewNodeState;
static sNodePingType                NodePing;
static sNodeRunType                 NodeRun;


static char                         TEMP_BUFF[BUFFER_SIZE];
static char                         TX_BUFF[BUFFER_SIZE];





/*==========================================================================
static local functions 
===========================================================================*/
static void                         ReadStorage(void);
static void                         FactoryWriteStorage(void);
    

static void                         OnTxDone( void );
static void                         OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void                         OnTxTimeout( void );
static void                         OnRxTimeout( void );
static void                         OnRxError( void );

static void                         SetNodeTickTimer(uint32_t time);
static void                         OnNodeTickTimer(void);

static void                         NodeStateMechine(sNodeEventType sEvent);
static void                         AddNodeEvent(sNodeEventType sEvent);

/* AT COMMAND CallBack Function ------------------------------------------------*/
static ATEerror_t                   OnATComEnergyScan(uint32_t interval);
static ATEerror_t                   OnATComEnergyScanAll(uint32_t interval);
static ATEerror_t                   OnATComReset(void);
static ATEerror_t                   OnATComFSet(void);
static ATEerror_t                   OnATComChannel(uint32_t channel);
static ATEerror_t                   OnATComRateset(uint32_t rateset);
static ATEerror_t                   OnATComBaudrate(uint32_t baudrate);
static ATEerror_t                   OnATComPing(uint16_t destNode, uint8_t len, uint32_t interval, uint32_t count);
static ATEerror_t                   OnATComSend(uint16_t destNode, uint8_t len, char* tx_data, uint8_t isNullModem, bool isGaro);
static ATEerror_t                   OnATComSleep(uint32_t nSecond);
extern ATEerror_t                   OnATComRxData(void);






extern void                         LoRaP2PDecrypt(uint8_t *srcBuffer, uint16_t size, uint8_t *key, uint8_t *decBuffer);
extern void                         LoRaP2PEncript(uint8_t *srcBuffer, uint16_t size, uint8_t *key, uint8_t *decBuffer);
extern void                         NVIC_SystemReset(void);
extern void                         SetSleepEnter(uint32_t nSecond);




#define CLEAR_NODE_EVENT()          (NodeEvent.eEvent = NODE_NONE_E)



/*==========================================================================
Global functions 
===========================================================================*/



sNodeEventType EVENT(eNodeEventType eEvent, uint32_t uParam1, uint32_t uParam2)
{
    sNodeEventType sEvent;
    memset(&sEvent, NULL, sizeof(sNodeEventType));
    
    sEvent.eEvent = eEvent;
    sEvent.uParam[0] = uParam1;
    sEvent.uParam[1] = uParam2;

    return sEvent;
}




sAppInfoType *App_Initialize(void)
{
    memset(&NodeEvent, NULL, sizeof(NodeEvent));
    memset(&NodeAppInfo, NULL, sizeof(NodeAppInfo));
 
    strcpy(NodeAppInfo.sVersion, P2P_VERSION_STRING);

    ReadStorage();
    
    NodeAppInfo.SetPing = OnATComPing;
    NodeAppInfo.SetChannel = OnATComChannel;
    NodeAppInfo.SetRateset = OnATComRateset;
    NodeAppInfo.SetBaudrate = OnATComBaudrate;
    NodeAppInfo.SetEnergyScan = OnATComEnergyScan;
    NodeAppInfo.SetEnergyScanAll = OnATComEnergyScanAll; 
    NodeAppInfo.SetReset = OnATComReset;
    NodeAppInfo.SetFSet = OnATComFSet;
    NodeAppInfo.SetPing = OnATComPing;
    NodeAppInfo.SetSend = OnATComSend;
    NodeAppInfo.SetSleep = OnATComSleep;
    NodeAppInfo.GetRxData = OnATComRxData;

    RadioEvents.TxDone      = OnTxDone;
    RadioEvents.RxDone      = OnRxDone;
    RadioEvents.TxTimeout   = OnTxTimeout;
    RadioEvents.RxTimeout   = OnRxTimeout;
    RadioEvents.RxError     = OnRxError;
    
    P2P_API_INIT(&RadioEvents, NodeAppInfo.u8Channel, NodeAppInfo.u8Power, NodeAppInfo.u8Rateset);
    
    /* AT COMMAND */
    SetATCommandInfo((void*)&NodeAppInfo);
    
    TimerInit( &NodeTickTimer, OnNodeTickTimer);

    return &NodeAppInfo;
    
}




void App_Start(void)
{
    /* State Mechine */
    CurNodeState = NODE_ENTER_S;
    NodeStateMechine(EVENT(NODE_START_E,0,0));
}


void App_SleepWakeup(void)
{
    SetNodeTickTimer(NODE_TICK_TIME_INT);
    AddNodeEvent(EVENT(NODE_SLEEP_WAKE_E,0,0));
        
}




static void ReadStorage(void)
{
    uint8_t     factoryReset = false;
    uint32_t    u32StoSize = 0;
    
    ewbm_lora_storage_readb(LORA_U08_AES_PWD, NodeAppInfo.a16AesPwd);
    ewbm_lora_storage_readb(LORA_U08_DBGL, &NodeAppInfo.u8Dbgl);
    ewbm_lora_storage_readb(LORA_U08_CHANNEL, &NodeAppInfo.u8Channel);
    ewbm_lora_storage_readb(LORA_U08_RATESET, &NodeAppInfo.u8Rateset);
    ewbm_lora_storage_readb(LORA_U08_POWER, &NodeAppInfo.u8Power);
    ewbm_lora_storage_readb(LORA_U08_NULLMODEM, &NodeAppInfo.u8NullModem);
    ewbm_lora_storage_readb(LORA_U08_AES128, &NodeAppInfo.u8Aes128);
    ewbm_lora_storage_readb(LORA_U08_HOP_LEVEL, &NodeAppInfo.u8HopLevel);
    ewbm_lora_storage_readb(LORA_U08_RECV_FORMAT, &NodeAppInfo.u8RecvFormat);

    ewbm_lora_storage_readw(LORA_U16_PANID, &NodeAppInfo.u16PanID);
    ewbm_lora_storage_readw(LORA_U16_NODEID, &NodeAppInfo.u16NodeID);
    ewbm_lora_storage_readdw(LORA_U32_BAUDRATE, &NodeAppInfo.u32Baudrate);
    ewbm_lora_storage_readdw(LORA_U32_HOP_DELAY, &NodeAppInfo.u32HopDelay);
    ewbm_lora_storage_readdw(LORA_U32_STO_SIZE, &u32StoSize);

    if(u32StoSize != sizeof(NodeAppInfo)) {
        factoryReset = true;
    }
    else if(NodeAppInfo.u8Channel> 31) {
        factoryReset = true;
    }
    else if(NodeAppInfo.u8Power > 7) {
        factoryReset = true;
    }
    else if(NodeAppInfo.u16NodeID > 254) {
        factoryReset = true;
    }
    else {
        switch(NodeAppInfo.u32Baudrate) {
            case 9600 :
            case 19200 :
            case 38400 :
            case 57600 :
            case 115200 :
            case 230400 :
                break;
                
            default :
                factoryReset = true;
                break;
        }
    }
    

    if(factoryReset) {
        PRINTF("[MEMORY ERROIR, FACTORE RESET]\r\n");
        FactoryWriteStorage();
        NVIC_SystemReset();
    }
}



static void FactoryWriteStorage(void)
{
    uint8_t aesPwd[16] = {  0xAE, 0x11, 0x98, 0x7B, 0x46, 
                    0x47, 0x54, 0x66, 0xF4, 0x7E,    
                    0x06, 0x1C, 0x30, 0x99, 0x58, 0x24, };

    memset(&NodeAppInfo, NULL, sizeof(NodeAppInfo));

    /* Factory Reset, Default settiong */
    memcpy(NodeAppInfo.a16AesPwd, aesPwd, 16);
    NodeAppInfo.u8Channel = 31;
    NodeAppInfo.u8HopLevel = 0;
    NodeAppInfo.u16PanID = 1;
    NodeAppInfo.u16NodeID = 1;
    NodeAppInfo.u32Baudrate = 115200;
    NodeAppInfo.u32HopDelay = 0;

    ewbm_lora_storage_writeb(LORA_U08_AES_PWD, NodeAppInfo.a16AesPwd);
    ewbm_lora_storage_writeb(LORA_U08_DBGL, &NodeAppInfo.u8Dbgl);
    ewbm_lora_storage_writeb(LORA_U08_CHANNEL, &NodeAppInfo.u8Channel);
    ewbm_lora_storage_writeb(LORA_U08_RATESET, &NodeAppInfo.u8Rateset);
    ewbm_lora_storage_writeb(LORA_U08_POWER, &NodeAppInfo.u8Power);
    ewbm_lora_storage_writeb(LORA_U08_NULLMODEM, &NodeAppInfo.u8NullModem);
    ewbm_lora_storage_writeb(LORA_U08_AES128, &NodeAppInfo.u8Aes128);
    ewbm_lora_storage_writeb(LORA_U08_HOP_LEVEL, &NodeAppInfo.u8HopLevel);
    ewbm_lora_storage_writeb(LORA_U08_RECV_FORMAT, &NodeAppInfo.u8RecvFormat);
    
    ewbm_lora_storage_writew(LORA_U16_PANID, NodeAppInfo.u16PanID);
    ewbm_lora_storage_writew(LORA_U16_NODEID, NodeAppInfo.u16NodeID);
    ewbm_lora_storage_writedw(LORA_U32_BAUDRATE, NodeAppInfo.u32Baudrate);
    ewbm_lora_storage_writedw(LORA_U32_HOP_DELAY, NodeAppInfo.u32HopDelay);
    ewbm_lora_storage_writedw(LORA_U32_STO_SIZE, sizeof(NodeAppInfo));
    
}





static void SendRadioData(uint8_t *pData, int nSize)
{   
    if(NodeAppInfo.u8Aes128) {
        LoRaP2PEncript(pData, nSize, NodeAppInfo.a16AesPwd, (uint8_t*)TEMP_BUFF);
        P2P_API_SEND((uint8_t*)TEMP_BUFF, nSize);
    }
    else {
        P2P_API_SEND(pData, nSize);
    }
}


static void AddNodeEvent(sNodeEventType sEvent)
{
    BACKUP_PRIMASK();
    DISABLE_IRQ();
    
    if(NodeAppInfo.u8EventIndex < EVENT_QUEUE_COUNT) {
        NodeAppInfo.sEventQueue[NodeAppInfo.u8EventIndex++] = sEvent;
    }
    
    RESTORE_PRIMASK();

    NodeAppInfo.nLowTimerCnt = LOW_TIMER_CNT_RESET;
    SetNodeTickTimer(NODE_FAST_TIME_INT);
}





static void SetNodeTickTimer(uint32_t time)
{
    if(time == 0) {
        TimerStop(&NodeTickTimer);
    }
    else {
        TimerStop(&NodeTickTimer);
        TimerSetValue(&NodeTickTimer, time);
        TimerStart(&NodeTickTimer);
    }
}


#if 0
static int ParseRxData(char *sData, int dataLen, char *rxCmd, uint8_t *txSeqCount, uint16_t *srcNode, uint16_t *destNode, uint8_t *hopLevel, char *rxData, int *rxSize)
{
    int     size;
    char    sToken[50];
    char    *sStart, *sParse;
    
    if(sData[0] != '[') {
        return 1;
    }


    /* Command ----------------------------------
    ** SEND
    */
    
    sStart = (char*)&sData[1];
    sParse = strstr(sStart, ":");
    if(sParse == NULL) {
        return 2;
    }

    size = (int)(sParse - sStart);
    memcpy(rxCmd, sStart, size);
    rxCmd[size] = NULL;
    sStart += (size + 1);

    DPRINTF("[COMMAND:%s]\r\n", rxCmd);


    /* Tx Count -------------------------------*/
    if(dataLen < (int)(sStart-sData)) {
        return 3;
    }
    
    sParse = strstr(sStart, ",");
    if(sParse == NULL) {
        return 4;
    }

    size = (int)(sParse - sStart);
    memcpy(sToken, sStart, size);
    sToken[size] = NULL;
    *txSeqCount = atoi(sToken);
    sStart += (size + 1);
    DPRINTF("[TX-SEQ-NUM:%d]\r\n", *txSeqCount);


    /* dest Node ------------------------------*/
    if(dataLen < (int)(sStart-sData)) {
        return 5;
    }
    
    sParse = strstr(sStart, ",");
    if(sParse == NULL) {
        return 6;
    }

    size = (int)(sParse - sStart);
    memcpy(sToken, sStart, size);
    sToken[size] = NULL;
    *srcNode = atoi(sToken);
    sStart += (size + 1);
    DPRINTF("[SRC-NODE:%d]\r\n", *srcNode);

    
    /* soure Node ------------------------------*/
    if(dataLen < (int)(sStart-sData)) {
        return 7;
    }
    
    sParse = strstr(sStart, ",");
    if(sParse == NULL) {
        return 8;
    }
    size = (int)(sParse - sStart);
    memcpy(sToken, sStart, size);
    sToken[size] = NULL;
    *destNode = atoi(sToken);
    sStart += (size + 1);
    DPRINTF("[DEST-NODE:%d]\r\n", *destNode);
    
    
    /* hopLevel ------------------------------*/
    if(dataLen < (int)(sStart-sData)) {
        return 9;
    }
    
    sParse = strstr(sStart, ",");
    if(sParse == NULL) {
        return 10;
    }
    size = (int)(sParse - sStart);
    memcpy(sToken, sStart, size);
    sToken[size] = NULL;
    *hopLevel = atoi(sToken);
    sStart += (size + 1);
    DPRINTF("[HOP-LEVEL:%d]\r\n", *hopLevel);
    

    /* rxData ----------------------------------*/
    if(dataLen < (int)(sStart-sData)) {
        return 11;
    }
    
    int nPktSize = dataLen - ((int)(sStart-sData));
    
    
    memcpy(rxData, sStart, nPktSize);
    if(rxData[nPktSize-1] == ']') {
        nPktSize--;
    }
    
    *rxSize = nPktSize;

    DPRINTF("[RXDTA : Size=%d : ", nPktSize);
    for(int i=0; i<nPktSize; i++) {
        DPRINTF("%02X,", (uint8_t)rxData[i]);
    }
    DPRINTF("]\r\n");

    return 0;

}
#else
static int ParseRxData(char *sData, int dataLen, char *rxCmd, uint8_t *txSeqCount, uint16_t *srcNode, uint16_t *destNode, uint8_t *hopLevel, char *rxData, int *rxSize)
{
    int     i = 0;
    
    if(sData[i++] != '[') {
        return 1;
    }


    /* Command ----------------------------------
    ** SEND
    */
    
    rxCmd[0] = (char)sData[i++];
    if(rxCmd[0] == 'S') {
        strcpy(rxCmd, "SEND");
    }
    else if(rxCmd[0] == 'L') {
        strcpy(rxCmd, "SENL");
    }
    else if(rxCmd[0] == 'P') {
        strcpy(rxCmd, "PING");
    }
    else if(rxCmd[0] == 'O') {
        strcpy(rxCmd, "PONG");
    }
    DPRINTF("[COMMAND:%s]\r\n", rxCmd);
    

    *txSeqCount = sData[i++];
    *txSeqCount |= (sData[i++] << 8);

    DPRINTF("[TX-SEQ-NUM:%d]\r\n", *txSeqCount);


    /* Source Node ----------------------------*/
    *srcNode = sData[i++];
    *srcNode |= (sData[i++] << 8);
    DPRINTF("[SRC-NODE:%d]\r\n", *srcNode);

    
    /* Dest Node ------------------------------*/
    *destNode = sData[i++];
    *destNode |= (sData[i++] << 8);
    DPRINTF("[DEST-NODE:%d]\r\n", *destNode);

    
    /* Hop Node ------------------------------*/
    *hopLevel = sData[i++];
    *hopLevel |= (sData[i++] << 8);
    DPRINTF("[HOP-LEVEL:%d]\r\n", *hopLevel);
    

    /* rxData --------------------------------*/
    if(dataLen < i) {
        return 1;
    }
    
    int nPktSize = (dataLen - i);
    memcpy(rxData, &sData[i], nPktSize);
    if(rxData[nPktSize-1] == ']') {
        nPktSize--;
    }
    
    *rxSize = nPktSize;

    DPRINTF("[RXDTA : Size=%d : ", nPktSize);
    for(int i=0; i<nPktSize; i++) {
        DPRINTF("%02X,", (uint8_t)rxData[i]);
    }
    DPRINTF("]\r\n");

    return 0;

}

#endif


static void OnTxDone( void )
{
    AddNodeEvent(EVENT(NODE_TX_DONE_E, 0, 0));
}


static void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
    uint8_t         txSeqCount;
    uint16_t        srcNode;
    uint16_t        destNode;
    uint8_t         hopLevel;
    uint8_t         isHopWorking;
    uint8_t         isNullModem;
    char            sCommand[20];
    int             nRxDataSize;
    sNodeEventType  sEvent;

    if(size == 0) {
        DPRINTF("[OnRxDone size err1]\r\n");
        return;
    }

    else if(size > BUFFER_SIZE) {
        DPRINTF("[OnRxDone size err2]\r\n");
        return;
    }

    memset(&sEvent, NULL, sizeof(sEvent));
    
    if(NodeAppInfo.u8Aes128) {
        memcpy(TEMP_BUFF, payload, size);
        LoRaP2PDecrypt((uint8_t*)TEMP_BUFF, size, NodeAppInfo.a16AesPwd, payload);
    }
    
    payload[size] = NULL;

    int nParse = ParseRxData((char*)payload, size, sCommand, &txSeqCount, &srcNode, &destNode, &hopLevel, sEvent.sData, &nRxDataSize);
    if(nParse != 0) {
        DPRINTF("[OnRxDone Parse err %d]\r\n", nParse);
        return;
    }

    
    if(srcNode == NodeAppInfo.u16NodeID) {
        DPRINTF("[Sender Skip %d]\r\n", srcNode);
        return;
    
    }
    
    if(NodeAppInfo.u8PreRxSeqCount == txSeqCount) {

        if(NodeAppInfo.nPreRxDataSize == nRxDataSize && memcmp(NodeAppInfo.sPreRxBuff, sEvent.sData, nRxDataSize) == 0) {
            DPRINTF("[Duplicate Rx Seq data %d]\r\n", txSeqCount);
            return;
        }
    }
    if(NodeAppInfo.u8LastTxSeqCount == txSeqCount) {
        
        if(NodeAppInfo.nLastTxDataSize == nRxDataSize && strcmp(NodeAppInfo.sLastTxBuff, sEvent.sData) == 0) {
            DPRINTF("[Duplicate Tx Seq %d]\r\n", txSeqCount);
            return;
        }
    }

    
    NodeAppInfo.u8PreRxSeqCount = txSeqCount;

    NodeAppInfo.nPreRxDataSize = nRxDataSize;
    memcpy(NodeAppInfo.sPreRxBuff, sEvent.sData, nRxDataSize);
    
    
    
    P2P_API_RX_DISABLE();

    
    sEvent.nParam[0] = rssi;
    sEvent.nParam[1] = snr;

    isHopWorking = false;

    if(strcmp(sCommand, "PING") == 0) {
        if(destNode == NodeAppInfo.u16NodeID) {
            sEvent.eEvent = NODE_PONG_REQ_E;
            sEvent.uParam[0] = srcNode;
            sEvent.uParam[1] = strlen(sEvent.sData)-1;
            AddNodeEvent(sEvent);
        }
    }
    else if(strcmp(sCommand, "PONG") == 0) {
        if(destNode == NodeAppInfo.u16NodeID) {
            sEvent.eEvent = NODE_PONG_RSP_E;
            sEvent.uParam[0] = srcNode;
            sEvent.uParam[1] = TimerGetElapsedTime(NodePing.sendTime);
            AddNodeEvent(sEvent);
        }

    }
    else if(strcmp(sCommand, "SEND") == 0 || strcmp(sCommand, "SENL") == 0) {

        if(strcmp(sCommand, "SEND") == 0) {
            isNullModem = 0;
        }
        else {
            isNullModem = 1;
        }
    
        /* Master Node */
        int pktSize = 0;
        if((destNode == NodeAppInfo.u16NodeID) || (destNode == 0xFFFF)) {
            if(isNullModem == 0) {
                if(NodeAppInfo.u8RecvFormat == 1) {
                    sprintf(NodeAppInfo.sLastRxBuff, "[AT+RECV=%d,%d,%d,", srcNode, rssi, nRxDataSize);
                    pktSize = strlen(NodeAppInfo.sLastRxBuff);
                
                    memcpy(&NodeAppInfo.sLastRxBuff[pktSize], sEvent.sData, nRxDataSize);
                    pktSize += nRxDataSize;
                    NodeAppInfo.sLastRxBuff[pktSize++] = ']';
                }
                else {
                    sprintf(NodeAppInfo.sLastRxBuff, "[");
                    pktSize = strlen(NodeAppInfo.sLastRxBuff);
                
                    memcpy(&NodeAppInfo.sLastRxBuff[pktSize], sEvent.sData, nRxDataSize);
                    pktSize += nRxDataSize;
                    NodeAppInfo.sLastRxBuff[pktSize++] = ']';
                }
            }
            else {
                memcpy(NodeAppInfo.sLastRxBuff, sEvent.sData, nRxDataSize);
                pktSize += nRxDataSize;
            }


            NodeAppInfo.sLastRxBuff[pktSize++] = '\r';
            NodeAppInfo.sLastRxBuff[pktSize++] = '\n';

            PRINTH((uint8_t*)NodeAppInfo.sLastRxBuff, pktSize);
            

            if(destNode == 0xFFFF && hopLevel > 0) {
                isHopWorking = true;
            }
        }
        else {
            if(hopLevel > 0 && NodeAppInfo.u16NodeID != 0) {
                isHopWorking = true;
            }
            else {
                DPRINTF("[SKIP DATA]\r\n");
            }
        }


        if(isHopWorking == true) {
            sEvent.eEvent = NODE_HOP_REQ_E;
            sEvent.uParam[0] = nRxDataSize;
            sEvent.uParam[1] = txSeqCount;
            sEvent.uParam[2] = srcNode;
            sEvent.uParam[3] = destNode;
            sEvent.uParam[4] = hopLevel-1;
            sEvent.uParam[5] = isNullModem;

            AddNodeEvent(sEvent);
        }
        
     }


    P2P_API_RX_ENABLE();

    
}

static void OnTxTimeout( void )
{
    // Radio.Sleep( );
    DPRINTF("[LoRa OnTxTimeout]\r\n");
    AddNodeEvent(EVENT(NODE_TX_ERR_E, NULL, NULL));	
}

static void OnRxTimeout( void )
{
    // Radio.Sleep( );
    DPRINTF("[LoRa OnRxTimeout]\r\n");
}

static void OnRxError( void )
{
    // Radio.Sleep( );
    DPRINTF("[LoRa OnRxError]\r\n");
    AddNodeEvent(EVENT(NODE_RX_ERR_E, NULL, NULL));	
}

static void OnNodeTickTimer(void)
{
    BACKUP_PRIMASK();
    DISABLE_IRQ();
        
    int nTickInterval = NODE_TICK_TIME_INT;
        
    for(uint8_t i=0; i<NodeAppInfo.u8EventIndex; i++) {
        NodeStateMechine(NodeAppInfo.sEventQueue[i]);
    }
    
    NodeAppInfo.u8EventIndex = 0;
    NodeStateMechine(EVENT(NODE_TICK_TIMER_E, NULL, NULL));	
    RESTORE_PRIMASK();

    

    if(CurNodeState == NODE_IDLING_S) {
        if(NodeAppInfo.nLowTimerCnt <= 0) {
            nTickInterval = 10000;
        }
        else {
            NodeAppInfo.nLowTimerCnt--;
        }
    }

    SetNodeTickTimer(nTickInterval);
    
}





static ATEerror_t OnATComEnergyScan(uint32_t interval)
{
    if(CurNodeState != NODE_IDLING_S) {
        return AT_BUSY_ERROR;
    }
    
    return AT_OK;
}


static ATEerror_t OnATComEnergyScanAll(uint32_t interval)
{
    if(CurNodeState != NODE_IDLING_S) {
        return AT_BUSY_ERROR;
    }

    return AT_OK;
}

static ATEerror_t OnATComReset(void)
{
    if(CurNodeState != NODE_IDLING_S) {
        return AT_BUSY_ERROR;
    }

    AddNodeEvent(EVENT(NODE_RESET_E, 0, 0));
    return AT_OK;
}

static ATEerror_t OnATComFSet(void)
{
    if(CurNodeState != NODE_IDLING_S) {
        return AT_BUSY_ERROR;
    }

    AddNodeEvent(EVENT(NODE_FSET_E, 0, 0));
    return AT_OK;
}




/* ==========================================================================
** A T+PING=dest,len,interval,count
** d est: 목적지 노드 ID ( 254
** l en: 핑 메시지 길이 1~1 28 바이트
** i nterval: 핑 메시지 송신 주기 1~9999 초 , 통신속도에 따라 설정
** c ount: 핑 메시지 송신 횟수 0 ~99999999 , 0 은 핑 테스트 중지
** ==========================================================================*/
static ATEerror_t OnATComPing(uint16_t destNode, uint8_t len, uint32_t interval, uint32_t count)
{
    sNodeEventType sEvent;

    sEvent.eEvent = NODE_PING_REQ_E;
    sEvent.uParam[0] = destNode;
    sEvent.uParam[1] = len;
    sEvent.uParam[2] = interval;
    sEvent.uParam[3] = count;

    DPRINTF("[OnATComPing %d, %d, %d, %d]\r\n", destNode, len, interval, count);

    if(CurNodeState != NODE_IDLING_S) {
        return AT_BUSY_ERROR;
    }

    AddNodeEvent(sEvent);
    return AT_OK;
}


static ATEerror_t OnATComChannel(uint32_t channel)
{

    if(CurNodeState != NODE_IDLING_S) {
        return AT_BUSY_ERROR;
    }
    
    NodeAppInfo.u8Channel = (uint8_t)channel;
    ewbm_lora_storage_writeb(LORA_U08_CHANNEL, &NodeAppInfo.u8Channel);
    
    P2P_API_RX_DISABLE(); 

    P2P_API_RADIO_INIT(NodeAppInfo.u8Channel, NodeAppInfo.u8Power, NodeAppInfo.u8Rateset);
    
    HAL_Delay(50);
    
    P2P_API_RX_ENABLE(); 
    
    return AT_OK;
}


static ATEerror_t OnATComRateset(uint32_t rateset)
{
    if(CurNodeState != NODE_IDLING_S) {
        return AT_BUSY_ERROR;
    }
    
    NodeAppInfo.u8Rateset = (uint8_t)rateset;
    ewbm_lora_storage_writeb(LORA_U08_RATESET, &NodeAppInfo.u8Rateset);
    
    P2P_API_RX_DISABLE(); 
    P2P_API_RADIO_INIT(NodeAppInfo.u8Channel, NodeAppInfo.u8Power, NodeAppInfo.u8Rateset);
    
    HAL_Delay(50);
    
    P2P_API_RX_ENABLE(); 
    
    return AT_OK;
}



static ATEerror_t OnATComBaudrate(uint32_t baudrate)
{
    sNodeEventType sEvent;
    if(CurNodeState != NODE_IDLING_S) {
        return AT_BUSY_ERROR;
    }
    
    sEvent.eEvent = NODE_BAUDRATE_E;
    sEvent.uParam[0] = baudrate;
    AddNodeEvent(sEvent);

    
    return AT_OK;
}






static ATEerror_t OnATComSend(uint16_t destNode, uint8_t len, char* tx_data, uint8_t isNullModem, bool isGaro)
{
    sNodeEventType sEvent;
    
    if(CurNodeState != NODE_IDLING_S) {
        return AT_BUSY_ERROR;
    }

    sEvent.eEvent = NODE_SEND_REQ_E;
    memcpy(sEvent.sData, tx_data, len);
    sEvent.uParam[0] = len;
    sEvent.uParam[1] = destNode;
    sEvent.uParam[2] = isNullModem;

        
    AddNodeEvent(sEvent);
    
    return AT_OK;

}


static ATEerror_t OnATComSleep(uint32_t nSecond)
{
    sNodeEventType sEvent;
    
    if(CurNodeState != NODE_IDLING_S) {
        return AT_BUSY_ERROR;
    }

    sEvent.eEvent = NODE_SLEEP_E;
    sEvent.uParam[0] = nSecond;

    AddNodeEvent(sEvent);
    
    return AT_OK;

}

static ATEerror_t OnATComRxData(void)
{
    PRINTF("[%s]\r\n", NodeAppInfo.sLastRxBuff);
    return AT_OK;

}


#if 0
static bool SendNodeUserData(void)
{
    NodeAppInfo.u8LastTxSeqCount = NodeAppInfo.u8TxSeqCount;
    NodeAppInfo.nLastTxDataSize = NodeRun.uParam[0];
    memcpy(NodeAppInfo.sLastTxBuff, NodeRun.data, NodeRun.uParam[0]);
    
    
    if(NodeRun.isNullModem) {
        sprintf(TX_BUFF, "[SENL:%d,%d,%d,%d,", NodeAppInfo.u8TxSeqCount++, NodeAppInfo.u16NodeID, NodeRun.destNode, NodeAppInfo.u8HopLevel);
    }
    else {
        sprintf(TX_BUFF, "[SEND:%d,%d,%d,%d,", NodeAppInfo.u8TxSeqCount++, NodeAppInfo.u16NodeID, NodeRun.destNode, NodeAppInfo.u8HopLevel);
    }
   
    int nPktSize = strlen(TX_BUFF);
    memcpy(&TX_BUFF[nPktSize], NodeRun.data, NodeRun.uParam[0]);
    nPktSize += NodeRun.uParam[0];
    TX_BUFF[nPktSize++] = ']';
    
    uint32_t maxSize = P2P_API_GET_MAX_PAYLOAD(NodeAppInfo.u8Rateset);

    if(maxSize < nPktSize) {
        return false;
    }

    if(NodeAppInfo.u8TxSeqCount >= 254) {
        NodeAppInfo.u8TxSeqCount = 1;
    }
   
    P2P_API_RADIO_TX_CONFIG(NodeAppInfo.u8Power, NodeAppInfo.u8Rateset);

    SendRadioData((uint8_t*)TX_BUFF, nPktSize);
        
    return true;
}
#else
static bool SendNodeUserData(void)
{
    int i=0;
    NodeAppInfo.u8LastTxSeqCount = NodeAppInfo.u8TxSeqCount;
    NodeAppInfo.nLastTxDataSize = NodeRun.uParam[0];
    memcpy(NodeAppInfo.sLastTxBuff, NodeRun.data, NodeRun.uParam[0]);
    
    
    if(NodeRun.isNullModem) {
        TX_BUFF[i++] = '[';
        TX_BUFF[i++] = 'L';
    }
    else {
        
        TX_BUFF[i++] = '[';
        TX_BUFF[i++] = 'S';
    }


    TX_BUFF[i++] = (uint8_t)(NodeAppInfo.u8TxSeqCount & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeAppInfo.u8TxSeqCount >> 8) & 0xFF);
    NodeAppInfo.u8TxSeqCount++;

    TX_BUFF[i++] = (uint8_t)(NodeAppInfo.u16NodeID & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeAppInfo.u16NodeID >> 8) & 0xFF);


    TX_BUFF[i++] = (uint8_t)(NodeRun.destNode & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeRun.destNode >> 8) & 0xFF);


    TX_BUFF[i++] = (uint8_t)(NodeAppInfo.u8HopLevel & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeAppInfo.u8HopLevel >> 8) & 0xFF);


    memcpy(&TX_BUFF[i], NodeRun.data, NodeRun.uParam[0]);
    i += NodeRun.uParam[0];
    TX_BUFF[i++] = ']';
    
    uint32_t maxSize = P2P_API_GET_MAX_PAYLOAD(NodeAppInfo.u8Rateset);

    if(maxSize < i) {
        return false;
    }

    if(NodeAppInfo.u8TxSeqCount >= 254) {
        NodeAppInfo.u8TxSeqCount = 1;
    }
   
    P2P_API_RADIO_TX_CONFIG(NodeAppInfo.u8Power, NodeAppInfo.u8Rateset);

    SendRadioData((uint8_t*)TX_BUFF, i);
        
    return true;
}

#endif




static bool SendNodeHopData(void)
{
    int i=0;    
    if(NodeRun.isNullModem) {
        TX_BUFF[i++] = '[';
        TX_BUFF[i++] = 'L';
    }
    else {
        
        TX_BUFF[i++] = '[';
        TX_BUFF[i++] = 'S';
    }
    

    TX_BUFF[i++] = (uint8_t)(NodeRun.uParam[1] & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeRun.uParam[1] >> 8) & 0xFF);

    TX_BUFF[i++] = (uint8_t)(NodeRun.uParam[2] & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeRun.uParam[2] >> 8) & 0xFF);

    TX_BUFF[i++] = (uint8_t)(NodeRun.uParam[3] & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeRun.uParam[3] >> 8) & 0xFF);

    TX_BUFF[i++] = (uint8_t)(NodeRun.uParam[4] & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeRun.uParam[4] >> 8) & 0xFF);

   
    memcpy(&TX_BUFF[i], NodeRun.data, NodeRun.uParam[0]);
    i += NodeRun.uParam[0];
    TX_BUFF[i++] = ']';

    
    uint32_t maxSize = P2P_API_GET_MAX_PAYLOAD(NodeAppInfo.u8Rateset);

    if(maxSize < i) {
        return false;
    }
   
    P2P_API_RADIO_TX_CONFIG(NodeAppInfo.u8Power, NodeAppInfo.u8Rateset);
    SendRadioData((uint8_t*)TX_BUFF, i);
        
    return true;
}



#if 0
static void SendNodePingData(void)
{
    if(NodePing.len > 120) {
        NodePing.len = 120;
    }
    else if(NodePing.len < 1) {
        NodePing.len = 1;
    }
    
    memset(TEMP_BUFF, 'P', NodePing.len);
    TEMP_BUFF[NodePing.len] = NULL;

    
    sprintf(TX_BUFF, "[PING:%d,%d,%d,%d,%s]", NodeAppInfo.u8TxSeqCount++, NodeAppInfo.u16NodeID, NodePing.destNode, 0, TEMP_BUFF);
    if(NodeAppInfo.u8TxSeqCount >= 254) {
        NodeAppInfo.u8TxSeqCount = 1;
    }

    P2P_API_RADIO_TX_CONFIG(NodeAppInfo.u8Power, NodeAppInfo.u8Rateset);
    SendRadioData((uint8_t*)TX_BUFF, strlen(TX_BUFF));
    
}
#else
static void SendNodePingData(void)
{
    int i=0;
    if(NodePing.len > 120) {
        NodePing.len = 120;
    }
    else if(NodePing.len < 1) {
        NodePing.len = 1;
    }
    
    memset(TEMP_BUFF, 'P', NodePing.len);
    TEMP_BUFF[NodePing.len] = NULL;

   
    TX_BUFF[i++] = '[';
    TX_BUFF[i++] = 'P';
    
    TX_BUFF[i++] = (uint8_t)(NodeAppInfo.u8TxSeqCount & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeAppInfo.u8TxSeqCount >> 8) & 0xFF);
    
    TX_BUFF[i++] = (uint8_t)(NodeAppInfo.u16NodeID & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeAppInfo.u16NodeID >> 8) & 0xFF);

    TX_BUFF[i++] = (uint8_t)(NodePing.destNode & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodePing.destNode >> 8) & 0xFF);

    TX_BUFF[i++] = 0;
    TX_BUFF[i++] = 0;

    memcpy(&TX_BUFF[i], TEMP_BUFF, NodePing.len);
    i += NodePing.len;
    TX_BUFF[i++] = ']';


    NodeAppInfo.u8TxSeqCount++;
    if(NodeAppInfo.u8TxSeqCount >= 254) {
        NodeAppInfo.u8TxSeqCount = 1;
    }

    P2P_API_RADIO_TX_CONFIG(NodeAppInfo.u8Power, NodeAppInfo.u8Rateset);
    SendRadioData((uint8_t*)TX_BUFF, i);
    
}
#endif



#if 0
static void SendNodePongData(void)
{
    sprintf(TX_BUFF, "[PONG:%d,%d,%d,%d,%s]", NodeAppInfo.u8TxSeqCount++, NodeAppInfo.u16NodeID, NodePing.destNode, 0, NodePing.data);
    if(NodeAppInfo.u8TxSeqCount >= 254) {
        NodeAppInfo.u8TxSeqCount = 1;
    }

    P2P_API_RADIO_TX_CONFIG(NodeAppInfo.u8Power, NodeAppInfo.u8Rateset);

    DPRINTF("[TX %s]\r\n", TX_BUFF);
    
    SendRadioData((uint8_t*)TX_BUFF, strlen(TX_BUFF));
    
}
#else
static void SendNodePongData(void)
{
    int i = 0;
    
    TX_BUFF[i++] = '[';
    TX_BUFF[i++] = 'O';
    
    TX_BUFF[i++] = (uint8_t)(NodeAppInfo.u8TxSeqCount & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeAppInfo.u8TxSeqCount >> 8) & 0xFF);
    
    TX_BUFF[i++] = (uint8_t)(NodeAppInfo.u16NodeID & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodeAppInfo.u16NodeID >> 8) & 0xFF);

    TX_BUFF[i++] = (uint8_t)(NodePing.destNode & 0xFF);
    TX_BUFF[i++] = (uint8_t)((NodePing.destNode >> 8) & 0xFF);

    TX_BUFF[i++] = 0;
    TX_BUFF[i++] = 0;

    int nDataLen = strlen(NodePing.data);
    memcpy(&TX_BUFF[i], NodePing.data, nDataLen);
    i += nDataLen;
    
    TX_BUFF[i++] = ']';


    
    if(NodeAppInfo.u8TxSeqCount >= 254) {
        NodeAppInfo.u8TxSeqCount = 1;
    }

    P2P_API_RADIO_TX_CONFIG(NodeAppInfo.u8Power, NodeAppInfo.u8Rateset);

    SendRadioData((uint8_t*)TX_BUFF, strlen(TX_BUFF));
    
}

#endif


static void SetFactoryReset(void)
{
    FactoryWriteStorage();
    NVIC_SystemReset();
}

static void NodeStateMechine(sNodeEventType sEvent)
{
    uint32_t        u32Value;
    
    NewNodeState = CurNodeState;
    NodeEvent = sEvent;
    
    while(NewNodeState != NODE_NONE_S) {

        if(CurNodeState != NewNodeState) {
            // DPRINTF("[NodeStateMechine Cur=%d,  New=%d  Event=%d]\r\n", CurNodeState, NewNodeState, NodeEvent.eEvent);
        }

        CurNodeState = NewNodeState;
        NewNodeState = NODE_NONE_S;


        switch(CurNodeState) {
            case NODE_ENTER_S :
                if(NodeEvent.eEvent == NODE_START_E) {
                    SetNodeTickTimer(NODE_TICK_TIME_INT);
                    NewNodeState = NODE_IDLE_INIT_S;
                }
                CLEAR_NODE_EVENT();
                break;


            case NODE_IDLE_INIT_S :
                P2P_API_RADIO_INIT(NodeAppInfo.u8Channel, NodeAppInfo.u8Power, NodeAppInfo.u8Rateset);
                P2P_API_RX_ENABLE();

                NodeAppInfo.nLowTimerCnt = LOW_TIMER_CNT_RESET;
                NewNodeState = NODE_IDLING_S;
                CLEAR_NODE_EVENT();
                break;
                


            case NODE_IDLING_S :
                switch(NodeEvent.eEvent) {
                    case NODE_SEND_REQ_E :
                        NodeRun.nRetry = 0;

                        memcpy(NodeRun.data, NodeEvent.sData, NodeEvent.uParam[0]);
                        NodeRun.uParam[0]   = NodeEvent.uParam[0];
                        NodeRun.destNode    = NodeEvent.uParam[1];
                        NodeRun.isNullModem = NodeEvent.uParam[2];
                        
                        NewNodeState = NODE_SEND_REQ_S;
                        break;

                       
                    case NODE_HOP_REQ_E :
                        NodeRun.nRetry = 0;
                        NodeRun.uParam[0] = NodeEvent.uParam[0];
                        NodeRun.uParam[1] = NodeEvent.uParam[1];
                        NodeRun.uParam[2] = NodeEvent.uParam[2];
                        NodeRun.uParam[3] = NodeEvent.uParam[3];
                        NodeRun.uParam[4] = NodeEvent.uParam[4];
                        NodeRun.isNullModem = NodeEvent.uParam[5];
                        
                        memcpy(NodeRun.data, NodeEvent.sData, NodeRun.uParam[0]);

                        NodeRun.tickCount = 0;
                        NewNodeState = NODE_HOP_REQ_S;
                        break;
                
                    case NODE_PING_REQ_E :
                        NodePing.index      = 0;
                        NodePing.nRetry     = 0;
                        NodePing.destNode   = NodeEvent.uParam[0];
                        NodePing.len        = NodeEvent.uParam[1];
                        NodePing.interval   = NodeEvent.uParam[2];
                        NodePing.count      = NodeEvent.uParam[3];

                        DPRINTF("[NODE_PING_REQ_E %d, %d, %d, %d]\r\n", NodePing.destNode, NodePing.len , NodePing.interval, NodePing.count);
                        
                        NewNodeState = NODE_PING_REQ_S;
                        break;

                    case NODE_PONG_REQ_E :
                        NodePing.index      = 0;
                        NodePing.nRetry     = 0;
                        NodePing.destNode   = NodeEvent.uParam[0];
                        NodePing.len        = NodeEvent.uParam[1];
                        
                        strncpy(NodePing.data, NodeEvent.sData, NodePing.len);
                        NodePing.data[NodePing.len] = NULL;
                        NewNodeState = NODE_PONG_REQ_S;
                        break;

                    
                    case NODE_RX_ERR_E :
                        P2P_API_RX_ENABLE();
                        break;

                    case NODE_BAUDRATE_E :
                        NodeRun.tickCount = 0;
                        NodeAppInfo.u32Baudrate = NodeEvent.uParam[0];
                        NewNodeState = NODE_BAUDRATE_S;
                        break;
                    
                    case NODE_RESET_E :
                        NodeRun.tickCount = 0;
                        NewNodeState = NODE_RESET_S;
                        break;

                       
                    case NODE_FSET_E :
                        NodeRun.tickCount = 0;
                        NewNodeState = NODE_FSET_S;
                        break;

                    case NODE_SLEEP_E :
                        NodeRun.tickCount = 0;
                        NodeRun.uParam[0] = NodeEvent.uParam[0];
                        NewNodeState = NODE_SLEEP_S;
                        break;
                    
                    default :
                         break;
                        
                }
                CLEAR_NODE_EVENT();
                break;
                


            case NODE_SEND_REQ_S :

                if(NodeRun.nRetry >= MAX_SEND_RETRY) {
                    NewNodeState = NODE_IDLE_INIT_S;
                    break;
                }

                if(NodeRun.destNode == NodeAppInfo.u16NodeID) {
                    PRINTF("[NODE ID DEST ERROR]\r\n");
                    NewNodeState = NODE_IDLE_INIT_S;
                    break;
                }

                P2P_API_RX_DISABLE();
                
                NodeRun.tickCount = 0;
                if(SendNodeUserData() == false) {
                    PRINTF("[TX MAX SIZE ERROR]\r\n");
                    NewNodeState = NODE_IDLE_INIT_S;
                    break;
                }
                NewNodeState = NODE_SEND_RSP_S;
                CLEAR_NODE_EVENT();
                break;
                

            case NODE_SEND_RSP_S :
                switch(NodeEvent.eEvent) {
                    case NODE_TICK_TIMER_E :
                        NodeRun.tickCount++;
                        if(IS_NODE_TIMEOUT(NodeRun.tickCount, 5000)) {
                            PRINTF("[TX SEND: TIMEOUT]\r\n");

                            NodeRun.nRetry++;
                            NewNodeState = NODE_SEND_REQ_S;
                        }
                        break;

                     case NODE_TX_DONE_E :
                        P2P_API_RX_ENABLE();
                        NodeRun.tickCount = 0;
                        PRINTF("[TX DONE]\r\n");
                        NewNodeState = NODE_IDLE_INIT_S;
                        break;

                     case NODE_TX_ERR_E :
                        NodeRun.nRetry++;
                        PRINTF("[TX SEND: ERROR (%d/%d)]\r\n", NodeRun.nRetry, MAX_SEND_RETRY);
                        NewNodeState = NODE_SEND_REQ_S;
                        break;
                     
                     case NODE_RX_ERR_E :
                         NodeRun.nRetry++;
                         PRINTF("[TX SEND: RX ERROR (%d/%d)]\r\n", NodeRun.nRetry, MAX_SEND_RETRY);
                         NewNodeState = NODE_SEND_REQ_S;
                         break;
                     
                     default :
                         break;
                        
                }
                CLEAR_NODE_EVENT();
                break;


            case NODE_HOP_REQ_S :
                switch(NodeEvent.eEvent) {
                    case NODE_TICK_TIMER_E :
                        
                        NodeRun.tickCount++;
                        u32Value = (NodeAppInfo.u32HopDelay);
                        if(u32Value == 0) {
                            u32Value = 10;
                        }
                        else if(u32Value > (30*1000)) {
                            u32Value = (30*1000);
                        }
                        
                        if(IS_NODE_TIMEOUT(NodeRun.tickCount, u32Value)) {

                            P2P_API_RX_DISABLE();
                            
                            NodeRun.tickCount = 0;
                            SendNodeHopData();
                            NewNodeState = NODE_HOP_RSP_S;
                            
                        }
                        break;

                    default :
                        break;
                }
                CLEAR_NODE_EVENT();
                break;


            case NODE_HOP_RSP_S :
                switch(NodeEvent.eEvent) {
                    case NODE_TICK_TIMER_E :
                        NodeRun.tickCount++;
                        if(IS_NODE_TIMEOUT(NodeRun.tickCount, 5000)) {
                            PRINTF("[HOP SEND: TIMEOUT]\r\n");

                            NodeRun.nRetry++;
                            NewNodeState = NODE_HOP_REQ_S;
                        }
                        break;

                     case NODE_TX_DONE_E :
                        P2P_API_RX_ENABLE();
                        NodeRun.tickCount = 0;
                        DPRINTF("[HOP DONE]\r\n");
                        NewNodeState = NODE_IDLE_INIT_S;
                        break;

                     case NODE_TX_ERR_E :
                        NodeRun.nRetry++;
                        PRINTF("[HOP SEND: ERROR (%d/%d)]\r\n", NodeRun.nRetry, MAX_SEND_RETRY);
                        NewNodeState = NODE_HOP_REQ_S;
                        break;
                     
                     case NODE_RX_ERR_E :
                         NodeRun.nRetry++;
                         PRINTF("[HOP SEND: RX ERROR (%d/%d)]\r\n", NodeRun.nRetry, MAX_SEND_RETRY);
                         NewNodeState = NODE_HOP_REQ_S;
                         break;
                     
                     default :
                         break;
                        
                }
                CLEAR_NODE_EVENT();
                break;


            case NODE_PING_REQ_S :
               
                if(NodePing.nRetry >= MAX_PING_RETRY) {
                    NewNodeState = NODE_IDLE_INIT_S;
                    break;
                }

                P2P_API_RX_DISABLE();

                NodePing.tickCount = 0;
                if(NodePing.index >= NodePing.count) {
                    NewNodeState = NODE_IDLE_INIT_S;
                }
                else {
                    PRINTF("[TX PING: REQ %d DEST %d LEN %d COUNT %d/%d]\r\n", 
                                NodeAppInfo.u8TxSeqCount, NodePing.destNode, NodePing.len, NodePing.index+1, NodePing.count);
                    
                    NodePing.sendTime = TimerGetCurrentTime();

                    SendNodePingData();
                    NewNodeState = NODE_PING_RSP_S;
                }
                CLEAR_NODE_EVENT();
                break;
                

            case NODE_PING_RSP_S :
                switch(NodeEvent.eEvent) {
                    case NODE_TICK_TIMER_E :
                        NodePing.tickCount++;
                        if(IS_NODE_TIMEOUT(NodePing.tickCount, NodePing.interval)) {
                            NodePing.index++;

                            PRINTF("[RX PONG: TIMEOUT]\r\n");
                            NewNodeState = NODE_PING_REQ_S;
                        }
                        break;

                     case NODE_TX_DONE_E :
                        P2P_API_RX_ENABLE();
                        NodePing.tickCount = 0;
                        break;

                     case NODE_TX_ERR_E :
                        NodePing.nRetry++;
                        PRINTF("[TX PING: TX ERROR (%d/%d)]\r\n", NodePing.nRetry, MAX_PING_RETRY);
                        NewNodeState = NODE_PING_REQ_S;
                        break;
                     
                     case NODE_RX_ERR_E :
                         NodePing.nRetry++;
                         PRINTF("[TX PING: RX ERROR (%d/%d)]\r\n", NodePing.nRetry, MAX_PING_RETRY);
                         NewNodeState = NODE_PING_REQ_S;
                         break;

                     case NODE_PONG_RSP_E :
                        PRINTF("[TX PONG: FROM %d VIA 1:1 HOP(s) RSSI:%d dBm, SNR:%d dbm COUNT 3 DELAY %d ms]\r\n",
                            NodeEvent.uParam[0], NodeEvent.nParam[0], NodeEvent.nParam[1], NodeEvent.uParam[1]);

                        NodePing.tickCount = 0;
                        NewNodeState = NODE_PING_NXT_S;
                        break;
                     
                     default :
                         break;
                        
                }
                CLEAR_NODE_EVENT();
                break;


            case NODE_PING_NXT_S :
                switch(NodeEvent.eEvent) {
                    case NODE_TICK_TIMER_E :
                        NodePing.tickCount++;
                        if(IS_NODE_TIMEOUT(NodePing.tickCount, 500)) {
                            NodePing.index++;
                            NewNodeState = NODE_PING_REQ_S;
                        }
                        break;
                        
                    default :
                         break;
                }
                CLEAR_NODE_EVENT();
                break;



            case NODE_PONG_REQ_S :
                
                P2P_API_RX_DISABLE();
                
                NodePing.tickCount = 0;
                if(NodePing.nRetry >= MAX_PING_RETRY) {
                    NewNodeState = NODE_IDLE_INIT_S;
                    break;
                }
            
                NodePing.sendTime = TimerGetCurrentTime();

                SendNodePongData();
                NewNodeState = NODE_PONG_RSP_S;
                CLEAR_NODE_EVENT();
                break;


            case NODE_PONG_RSP_S :
                switch(NodeEvent.eEvent) {
                    case NODE_TICK_TIMER_E :
                        NodePing.tickCount++;
                        if(IS_NODE_TIMEOUT(NodePing.tickCount, 2000)) {
                            NodePing.index++;
                            PRINTF("[TX PONG: TIMEOUT]\r\n");
                            NewNodeState = NODE_PING_REQ_S;
                        }
                        break;

                     case NODE_TX_DONE_E :
                        PRINTF("[TX DONE]\r\n");
                        NewNodeState = NODE_IDLE_INIT_S;
                        break;

                     case NODE_TX_ERR_E :
                        NodePing.nRetry++;
                        PRINTF("[TX PING: TX ERROR (%d/%d)]\r\n", NodePing.nRetry, MAX_PING_RETRY);
                        NewNodeState = NODE_PONG_REQ_S;
                        break;
                     
                     default :
                         break;

                }
                CLEAR_NODE_EVENT();
                break;


            case NODE_BAUDRATE_S :
                switch(NodeEvent.eEvent) {
                    case NODE_TICK_TIMER_E :
                        NodeRun.tickCount++;
                        if(IS_NODE_TIMEOUT(NodeRun.tickCount, 800)) {
                            ewbm_lora_storage_writedw(LORA_U32_BAUDRATE, NodeAppInfo.u32Baudrate);
                            NVIC_SystemReset();
                        }
                        break;
                        
                    default :
                         break;

                     
                }
                CLEAR_NODE_EVENT();
                break;


            case NODE_RESET_S :
                switch(NodeEvent.eEvent) {
                    case NODE_TICK_TIMER_E :
                        NodeRun.tickCount++;
                        if(IS_NODE_TIMEOUT(NodeRun.tickCount, 700)) {
                            NVIC_SystemReset();
                        }
                        break;
                        
                    default :
                         break;
                }
                CLEAR_NODE_EVENT();
                break;


            case NODE_FSET_S :
                switch(NodeEvent.eEvent) {
                    case NODE_TICK_TIMER_E :
                        NodeRun.tickCount++;
                        if(IS_NODE_TIMEOUT(NodeRun.tickCount, 700)) {
                            SetFactoryReset();
                        }
                        break;
                        
                    default :
                         break;
                }
                CLEAR_NODE_EVENT();
                break;


            case NODE_SLEEP_S :
                switch(NodeEvent.eEvent) {
                    case NODE_TICK_TIMER_E :
                        NodeRun.tickCount++;
                        if(IS_NODE_TIMEOUT(NodeRun.tickCount, 200)) {
                            SetSleepEnter(NodeRun.uParam[0]);
                            NewNodeState = NODE_SLEEP_WAKE_S;
                        }
                        break;
                        
                    default :
                         break;
                }
                CLEAR_NODE_EVENT();
                break;


            case NODE_SLEEP_WAKE_S :
                switch(NodeEvent.eEvent) {
                    case NODE_SLEEP_WAKE_E :
                        NewNodeState = NODE_IDLE_INIT_S;
                        break;
                        
                    default :
                         break;
                }
                CLEAR_NODE_EVENT();
                break;


            case NODE_EXIT_S :
                
                break;
            
            default :
                break;
        }
    }
}


void SetBatteryDisp(uint32_t battery)
{
	DPRINTF("Battery Level mV = %d \n\r", battery);
}

