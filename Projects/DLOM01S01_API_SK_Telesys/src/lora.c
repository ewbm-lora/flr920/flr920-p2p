
/* Includes ------------------------------------------------------------------*/
#include "lora.h"
#include "hw.h"
#include "low_power_manager.h"
#include "ewbm_lora_storage.h"
#include "timer.h"
#include "app.h"
#include "version.h"
#include "LoRaMacCrypto.h"
#include "at_ewbm.h"
#include "tiny_sscanf.h"




/* Private functions ---------------------------------------------------------*/
 
/* Lora Wan Spec -------------------------------------------------------------
  KR920-923MHz ISM band end-devices SHALL use the following default parameters
   � Default EIRP output power for end-device(920.9~921.9MHz): 10 dBm
   � Default EIRP output power for end-device(922.1~923.3MHz): 14 dBm
------------------------------------------------------------------------------*/

// BAND AREA : 917 - 923.5 MHz
// MAX 14 dBM 
#define RF_FREQUENCY_31                      923500000
#define RF_FREQUENCY_30                      923300000
#define RF_FREQUENCY_29                      923100000

#define RF_FREQUENCY_28                      922900000
#define RF_FREQUENCY_27                      922700000
#define RF_FREQUENCY_26                      922500000
#define RF_FREQUENCY_25                      922300000
#define RF_FREQUENCY_24                      922100000

// MAX 10 dBM 
#define RF_FREQUENCY_23                      921900000
#define RF_FREQUENCY_22                      921700000
#define RF_FREQUENCY_21                      921500000
#define RF_FREQUENCY_20                      921300000
#define RF_FREQUENCY_19                      921100000

#define RF_FREQUENCY_18                      920900000
#define RF_FREQUENCY_17                      920700000
#define RF_FREQUENCY_16                      920500000
#define RF_FREQUENCY_15                      920300000
#define RF_FREQUENCY_14                      920100000

#define RF_FREQUENCY_13                      919900000
#define RF_FREQUENCY_12                      919700000
#define RF_FREQUENCY_11                      919500000
#define RF_FREQUENCY_10                      919300000
#define RF_FREQUENCY_09                      919100000

#define RF_FREQUENCY_08                      918900000
#define RF_FREQUENCY_07                      918700000
#define RF_FREQUENCY_06                      918500000
#define RF_FREQUENCY_05                      918300000
#define RF_FREQUENCY_04                      918100000

#define RF_FREQUENCY_03                      917900000
#define RF_FREQUENCY_02                      917700000
#define RF_FREQUENCY_01                      917500000
#define RF_FREQUENCY_00                      917300000




#define RF_CHANNEL_START                    RF_FREQUENCY_0
#define RF_CHANNEL_LEVEL                    (FREQ_STEP_8*3)

#define TX_OUTPUT_POWER                     14          // dBm  0 ~ 14, MAX: 14
#define LORA_BANDWIDTH                      0           // [0: 125 kHz,
                                                        //  1: 250 kHz,
                                                        //  2: 500 kHz,
                                                        //  3: Reserved]
#define LORA_SPREADING_FACTOR               7           // [SF7..SF12]
#define LORA_CODINGRATE                     1           // [1: 4/5,
                                                        //  2: 4/6,
                                                        //  3: 4/7,
                                                        //  4: 4/8]
#define LORA_PREAMBLE_LENGTH                8           // Same for Tx and Rx   8
#define LORA_SYMBOL_TIMEOUT                 20          // 5 -> 20
#define LORA_FIX_LENGTH_PAYLOAD_ON          false
#define LORA_IQ_INVERSION_ON                false
#define LORA_CRC_ON                         true
#define LORA_TX_TIME_OUT                    100000       // 3000000



static uint8_t                              u8LastChannel;
static uint8_t                              u8LastPower;
static uint8_t                              u8LastRateset;


static uint32_t                             dataRateKor[] = {12, 11, 10, 9, 8, 7};
static const uint8_t                        MaxPayloadKR920[] = { 80, 80, 80, 180, 242, 242 };





void P2P_API_INIT(RadioEvents_t *pEvents, uint8_t u8Channel, uint8_t u8Power, uint8_t u8Rateset)
{
    Radio.Init( pEvents );

    u8LastChannel = u8Channel;
    u8LastPower = u8Power;
    u8LastRateset = u8Rateset;
    
    P2P_API_RADIO_INIT(u8LastChannel, u8LastPower, u8LastRateset);
   
    HAL_Delay(50);
    
    P2P_API_RX_ENABLE();  
}


void P2P_API_RADIO_INIT(uint8_t u8Channel, uint8_t u8Power, uint8_t u8Rateset)
{
    P2P_API_LORA_RADIO_CHANNEL(u8Channel);
    P2P_API_RADIO_TX_CONFIG(u8Power, u8Rateset);
    P2P_API_RADIO_RX_CONFIG(u8Rateset);
}

void P2P_API_RX_DISABLE(void)
{
    Radio.Sleep();
}


void P2P_API_RX_ENABLE(void)
{
    P2P_API_RADIO_INIT(u8LastChannel, u8LastPower, u8LastRateset);
    Radio.Rx(0);
}




uint8_t P2P_API_GET_MAX_POWER(void)
{
    return TX_OUTPUT_POWER;
}



uint32_t P2P_API_GET_MAX_PAYLOAD(uint8_t u8Rateset)
{
    uint32_t maxPayload = MaxPayloadKR920[u8Rateset];
    return maxPayload;
}

/* ==========================================================================
**  RS :       SF       Data
**  0  : SF12 /125kHz	 250
**  1  : SF11 /125kHz	 440
**  2  : SF10 /125kHz	 980
**  3  : SF9 /125kHz	1760
**  4  : SF8 /125kHz	3125
**  5  : SF7 /125kHz	5470
** ==========================================================================*/
uint32_t P2P_API_RADIO_TX_CONFIG(uint8_t u8Power, uint8_t u8Rateset)
{

    uint8_t     txpower      = TX_OUTPUT_POWER - u8Power;
    uint32_t    bandwidth    = 0;  // ??
    uint32_t    dataRate = dataRateKor[u8Rateset];

    Radio.SetTxConfig(  MODEM_LORA,	txpower, 0, bandwidth, dataRate,
                        LORA_CODINGRATE, LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
                        LORA_CRC_ON, 0, 0, LORA_IQ_INVERSION_ON, LORA_TX_TIME_OUT );

    uint32_t maxPayload = MaxPayloadKR920[u8Rateset];
    Radio.SetMaxPayloadLength( MODEM_LORA, maxPayload );

    return maxPayload;
}


void P2P_API_RADIO_RX_CONFIG(uint8_t u8Rateset)
{
    uint32_t    bandwidth    = 0;  // ??
    uint32_t    dataRate = dataRateKor[u8Rateset];
    
    Radio.SetRxConfig(MODEM_LORA, bandwidth, dataRate,
                    LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                    LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
                    0, LORA_CRC_ON, 0, 0, LORA_IQ_INVERSION_ON, true );
    
    uint32_t maxPayload = MaxPayloadKR920[u8Rateset];
    Radio.SetMaxPayloadLength( MODEM_LORA, maxPayload );
                    
}




void P2P_API_LORA_RADIO_CHANNEL(uint8_t u8Channel)
{
    uint32_t channel;
    uint32_t channels[32] = {
        RF_FREQUENCY_00, RF_FREQUENCY_01, RF_FREQUENCY_02, RF_FREQUENCY_03, RF_FREQUENCY_04,
        RF_FREQUENCY_05, RF_FREQUENCY_06, RF_FREQUENCY_07, RF_FREQUENCY_08, RF_FREQUENCY_09,
        
        RF_FREQUENCY_10, RF_FREQUENCY_11, RF_FREQUENCY_12, RF_FREQUENCY_13, RF_FREQUENCY_14,
        RF_FREQUENCY_15, RF_FREQUENCY_16, RF_FREQUENCY_17, RF_FREQUENCY_18, RF_FREQUENCY_19,
        
        RF_FREQUENCY_20, RF_FREQUENCY_21, RF_FREQUENCY_22, RF_FREQUENCY_23, RF_FREQUENCY_24,
        RF_FREQUENCY_25, RF_FREQUENCY_26, RF_FREQUENCY_27, RF_FREQUENCY_28, RF_FREQUENCY_29,

        RF_FREQUENCY_30, RF_FREQUENCY_31,
    };

    if(u8Channel > 31) {
        u8Channel = 31;
    }

    channel = channels[u8Channel];
    Radio.SetChannel(channel);
}


void P2P_API_SEND(uint8_t* pTxData, int nTxSize)
{    
    Radio.Send(pTxData, nTxSize);
}

