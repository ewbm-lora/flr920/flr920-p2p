/** 
  * @file     ewbm_lora_storage.c
  * @author   eWBM
  * @version  0.1
  * @date     9/17/2018
  * @brief    Key and etc information is stored into Flash, whole data is encrypt using Secure storage 
  * @attention
  *      Copyright(c) 2018 eWBM Co., Ltd. www.e-wbm.com
  *
  *       This file contains information that is proprietary to
  *      eWBM Co.,Ltd. and may not be distributed or copied
  *      without written consent from eWBM Co.,Ltd.
  *
  *      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  *      ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  *      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  *      IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  *      EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
  *      GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
  *      HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHERIN CONTRACT, STRICT LIABILITY, OR TORT
  *      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  * Brief history
  * -------------
  *  9/17/2018 : Draft by eWBM 
  */


/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "ewbm_lora_storage.h"
#include "vcom.h"
#include "aes.h"

#include "stm32l0xx_hal.h"

#define SS_DEBUG 0

/****** Secure Storage ************************************************************/
#define EWBM_SS_ID_DATA "eWBM"
#define EWBM_SS_ID_SIZE 4
#define EWBM_SS_FS_TB_SIZE 124
#define EWBM_SS_HEADER_SIZE (EWBM_SS_ID_SIZE + EWBM_SS_FS_TB_SIZE) 

#define SS_SAVED_TOKEN  '$'

#define EWBM_SS_HEADER_ADDRESS DATA_EEPROM_BASE
#define EWBM_SS_TABLE_ADDRESS (EWBM_SS_HEADER_ADDRESS+EWBM_SS_ID_SIZE)
#define EWBM_SS_DATA_ADDRESS (EWBM_SS_HEADER_ADDRESS + EWBM_SS_HEADER_SIZE + 8/*Margin*/) /* 0x8080088 */

#define EEPROM_END_ADDRESS (DATA_EEPROM_BANK2_END)

static uint8_t sstable[EWBM_SS_FS_TB_SIZE];

/**
 * @brief  Structure defining an LoRa Secure Storage
 */
struct LoRaSecureStorage_s {
  const uint32_t address; // memory address
  const uint8_t size; // data size
};

static const struct LoRaSecureStorage_s ssdata[] =
{
    // LORA_U08_DEVEUI
    {
        .address = EWBM_SS_DATA_ADDRESS,
        .size = 8,
    },
    // LORA_U08_AES_PWD_128
    {
        .address = EWBM_SS_DATA_ADDRESS + 8,
        .size = 16,
    },
    // LORA_U08_DBGL
    {
        .address = EWBM_SS_DATA_ADDRESS + 24,
        .size = 1,
    },
    // LORA_U08_CHANNEL
    {
        .address = EWBM_SS_DATA_ADDRESS + 25,
        .size = 1,
    },
    // LORA_U08_RATESET
    {
        .address = EWBM_SS_DATA_ADDRESS + 26,
        .size = 1,
    },
    // LORA_U08_POWER
    {
        .address = EWBM_SS_DATA_ADDRESS + 27,
        .size = 1,
    },
    // LORA_U08_NULLMODEM,
    {
        .address = EWBM_SS_DATA_ADDRESS + 28,
        .size = 1,
    },
    // LORA_U08_AES128
    {
        .address = EWBM_SS_DATA_ADDRESS + 29,
        .size = 1,
    },
    // LORA_U08_HOP_LEVEL
    {
        .address = EWBM_SS_DATA_ADDRESS + 30,
        .size = 1,
    },

    // LORA_U08_RECV_FORMAT
    {
        .address = EWBM_SS_DATA_ADDRESS + 31,
        .size = 1,
    },
    // LORA_U16_PANID
    {
        .address = EWBM_SS_DATA_ADDRESS + 32,
        .size = 2,
    },
    // LORA_U16_NODEID
    {
        .address = EWBM_SS_DATA_ADDRESS + 34,
        .size = 2,
    },
    // LORA_U32_BAUDRATE
    {
        .address = EWBM_SS_DATA_ADDRESS + 36,
        .size = 4,
    },
    
    // LORA_U32_HOP_DELAY
    {
        .address = EWBM_SS_DATA_ADDRESS + 40,
        .size = 4,
    },
    // LORA_U32_STO_SIZE
    {
        .address = EWBM_SS_DATA_ADDRESS + 44,
        .size = 4,
    },
};

/*!
 * Encryption aBlock and sBlock
 */
static uint8_t aBlock[] = { 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                          };
static uint8_t sBlock[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                          };

static void init_security_key()
{
  aes_context ctx;

  memset(&ctx, 0, sizeof(aes_context));
  
  ctx.ksch[0] = 'F';
  ctx.ksch[1] = 'L';
  ctx.ksch[2] = 'Y'; 
  ctx.ksch[3] = 'V';
  ctx.rnd = 1;

  for (uint8_t i=0; i < 12; i++)
  {
    aBlock[i] = *(volatile uint8_t *)(UID_BASE + i);
  }

  aes_encrypt( aBlock, sBlock, &ctx );

}

//--- Public functions ------------------------------------------------------------>>


void ewbm_lora_storage_init(void)
{
  /* Check SS id, if not invailed id then init. SS */
  uint32_t mid = *(__IO uint32_t*)EWBM_SS_HEADER_ADDRESS;
  uint32_t vid = *(uint32_t*)EWBM_SS_ID_DATA;

  memset(sstable, 0, EWBM_SS_FS_TB_SIZE);

  //if ( memcmp(&mid, &EWBM_SS_ID_DATA, EWBM_SS_ID_SIZE) )
  if ( mid != vid )
  {
    HAL_FLASHEx_DATAEEPROM_Unlock();
    HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, EWBM_SS_HEADER_ADDRESS, vid);

    for (uint8_t i=0; i < EWBM_SS_FS_TB_SIZE; ++i)
    {
      //HAL_FLASHEx_DATAEEPROM_Erase(EWBM_SS_HEADER_ADDRESS+EWBM_SS_ID_SIZE+i);
      HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, EWBM_SS_HEADER_ADDRESS+EWBM_SS_ID_SIZE+i, 0);
    }

     HAL_FLASHEx_DATAEEPROM_Lock();
  }
  else
  {
    for (uint8_t i=0; i < EWBM_SS_FS_TB_SIZE; i++)
    {
      sstable[i] = *(__IO uint8_t*)(EWBM_SS_TABLE_ADDRESS+i);
    }
  }

  init_security_key();
}

bool ewbm_lora_storage_readb(const LoraStorageItemType item, uint8_t* data/*, uint8_t* size*/)
{
  uint8_t encdata[16];

  if (sstable[(uint8_t)item] != SS_SAVED_TOKEN)
    return false;

  memset(encdata, 0, 16);

  //*size = ssdata[item].size;
  for (uint8_t i=0; i < ssdata[item].size; i++)
  {
    encdata[i] = *(__IO uint8_t*)(ssdata[item].address+i);
    data[i] = encdata[i] ^ sBlock[i];
  }

  return true;
}

bool ewbm_lora_storage_readw(const LoraStorageItemType item, uint16_t* data)
{
  uint8_t tmp[16];

  memset(tmp, 0, 16);

  if (!ewbm_lora_storage_readb(item, tmp))
    return false;

  *data = tmp[0];
  *data |= ( (uint32_t)tmp[1] << 8 );
  return true;
}


bool ewbm_lora_storage_readdw(const LoraStorageItemType item, uint32_t* data)
{
  uint8_t tmp[16];
  
  memset(tmp, 0, 16);

  if (!ewbm_lora_storage_readb(item, tmp))
    return false;

  *data= tmp[0];
  *data |= ( (uint32_t)tmp[1] << 8 );
  *data |= ( (uint32_t)tmp[2] << 16 );
  *data |= ( (uint32_t)tmp[3] << 24 );

  
  return true;
}


bool ewbm_lora_storage_writeb(const LoraStorageItemType item, const uint8_t* data/*, const uint8_t size*/)
{
  uint8_t encdata[16];

  /*if (ssdata[item].size != size)
    return false;*/

  memset(encdata, 0, 16);

  for (uint8_t i=0; i < ssdata[item].size; i++)
  {
    encdata[i] = data[i]  ^ sBlock[i];
  }

  HAL_FLASHEx_DATAEEPROM_Unlock();

  for (uint8_t i=0; i < ssdata[item].size; i++)
  {
    HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, ssdata[item].address+i , encdata[i]);
    HAL_Delay(30);
  }

  sstable[(uint8_t)item] = SS_SAVED_TOKEN;
  HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, EWBM_SS_TABLE_ADDRESS+((uint8_t)item) , SS_SAVED_TOKEN);
  HAL_Delay(30);

  HAL_FLASHEx_DATAEEPROM_Lock();

  return true;
}

bool ewbm_lora_storage_writew(const LoraStorageItemType item, const uint16_t data)
{
  uint8_t tmp[16];

  memset(tmp, 0, 16);

  tmp[0] = data & 0xff;
  tmp[1] = (data >> 8) & 0xff;

  return ewbm_lora_storage_writeb(item, tmp);
}

bool ewbm_lora_storage_writedw(const LoraStorageItemType item, const uint32_t data)
{
  uint8_t tmp[16];

  memset(tmp, 0, 16);

  tmp[0] = data & 0xff;
  tmp[1] = (data >> 8) & 0xff;
  tmp[2] = (data >> 16) & 0xff;
  tmp[3] = (data >> 24) & 0xff;

  return ewbm_lora_storage_writeb(item, tmp);
}



/****** User Storage ************************************************************/

bool ewbm_user_storage_readb(const uint32_t addr, uint8_t* data, const uint32_t len)
{
  if (addr < EWBM_USER_STORAGE_START_ADDRESS)
    return false;

  if ((addr+len) > EEPROM_END_ADDRESS)
    return false;

  for (uint32_t i=0; i < len; i++)
  {
    data[i] = *(__IO uint8_t*)(addr+i);
  }

  return true;
}

bool ewbm_user_storage_writeb(const uint32_t addr, const uint8_t* data, const uint32_t len)
{
  if (addr < EWBM_USER_STORAGE_START_ADDRESS)
    return false;

  if ((addr+len) > EEPROM_END_ADDRESS)
    return false;

  HAL_FLASHEx_DATAEEPROM_Unlock();

  for (uint32_t i=0; i < len; i++)
  {
    HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, addr+i , data[i]);
  }

  HAL_FLASHEx_DATAEEPROM_Lock();

  return true;
}



/* wjkim_2019_11_08 
** Factory Reset
*/
void eeprom_erase_all(void)
{
	memset(sstable, 0, EWBM_SS_FS_TB_SIZE);

	HAL_FLASHEx_DATAEEPROM_Unlock();

	for (uint8_t i=0; i < EWBM_SS_FS_TB_SIZE; ++i)
	{
		HAL_FLASHEx_DATAEEPROM_Erase(EWBM_SS_HEADER_ADDRESS+EWBM_SS_ID_SIZE+i);
	}

	HAL_FLASHEx_DATAEEPROM_Lock();
  
}

