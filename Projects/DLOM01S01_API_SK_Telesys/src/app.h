
//----------------------------------------------------------------------------
//    Project Name		: LoRa WAN
//----------------------------------------------------------------------------
//	File Name			: Main.h
//----------------------------------------------------------------------------
//	Processor			: STM32L071CZY    WLCSP
//	Compiler				: uVision V5.20.0.0
//	RF Transceiver	: SX1276
//	Frequency			:  917 ~ 928Mhz
//	Modulation			:  LoRa
//	Version				: H/W(1.3), LIB(1.01)
//	Company			: Wisol
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//	History
//	
//	DATE				Author			Version 		COMMENT
//	2018/06/01	KSH,CYH 		V1.00		Start
// 2016/07/31	 KSH,CYH		V1.01       	Modify : AFA, LRW 5D,Select Channel  Add : AU915, 
// 2016/08/01    KSH,CYH       V1.01         Add cmd: Channel Mask, Rx Delay1, Data rate, Join Accept Delay1, Repeater support, Activation, ABP setting value
//                                                                  Modify : JOIN_START, API_MODE                     
//----------------------------------------------------------------------------


#ifndef __APP_H
#define __APP_H

#include "stm32l0xx.h"
#include "at_ewbm.h"


#define FEATURE_BS
#define EVENT_QUEUE_COUNT           5


typedef enum {
    NODE_NONE_E,
        
    NODE_START_E,
    NODE_TICK_TIMER_E,
    
    NODE_RX_DONE_E,
    NODE_TX_DONE_E,
    NODE_RX_ERR_E,
    NODE_TX_ERR_E,

    NODE_SEND_REQ_E,
    NODE_HOP_REQ_E,
    
    NODE_PING_REQ_E,
    NODE_PONG_REQ_E,
    NODE_PONG_RSP_E,
    
    NODE_BAUDRATE_E,
    NODE_RESET_E,
    NODE_FSET_E,
    NODE_SLEEP_E,
    NODE_SLEEP_WAKE_E,
}eNodeEventType;



typedef struct {
    eNodeEventType  eEvent;

    int             nParam[6];
    uint32_t        uParam[6];
    char            sData[255];
}sNodeEventType;




typedef ATEerror_t      (*fNorExec)(void);
typedef ATEerror_t      (*fNorExecP1)(uint32_t param);
typedef ATEerror_t      (*fEnergyScan)(uint32_t interval);
typedef ATEerror_t      (*fPing)(uint16_t destNode, uint8_t len, uint32_t interval, uint32_t count);
typedef ATEerror_t      (*fSend)(uint16_t destNode, uint8_t len, char* tx_data, uint8_t isNullModem, bool isGaro);


typedef struct {
    char            sVersion[20];
    char            sTxBuff[130];
    
    uint8_t         u8TxSeqCount;
    
    uint8_t         u8PreRxSeqCount;
    int             nPreRxDataSize;
    char            sPreRxBuff[255];
    char            sLastRxBuff[255];

    uint8_t         u8LastTxSeqCount;
    int             nLastTxDataSize;
    char            sLastTxBuff[255];
    
    int             nLowTimerCnt;
    uint8_t         u8EventIndex;
    sNodeEventType  sEventQueue[EVENT_QUEUE_COUNT];
    
    /* Write Storage Start */    
    uint8_t         a16AesPwd[16];  
    uint8_t         u8Dbgl;                 /* Debug Level */
    uint8_t         u8Channel;              /* 0 ~ 31*/
    uint8_t         u8Rateset;              /* 0 ~ 5*/
    uint8_t         u8Power;                /* 0 ~ 7*/
    uint8_t         u8NullModem;            /* 0 : Release,  1: Set */
    uint8_t         u8Aes128;               /* 1: ON, 0: OFF*/
    uint8_t         u8HopLevel;             /* Relay Depth level */
    uint8_t         u8RecvFormat;

    uint16_t        u16PanID;               /* 0 ~ 65534*/
    uint16_t        u16NodeID;              /* 0 : Master */    
    uint32_t        u32Baudrate;            /* Uart boardrate */
    uint32_t        u32HopDelay;             /* Second, Max 30 Second */
    /* Write Storage End */    

    
    fEnergyScan     SetEnergyScan;
    fEnergyScan     SetEnergyScanAll;
    fNorExec        SetReset;
    fNorExec        SetFSet;
    fNorExecP1      SetChannel;
    fNorExecP1      SetRateset;
    fNorExecP1      SetBaudrate;
    fPing           SetPing;
    fSend           SetSend;
    fNorExecP1      SetSleep;
    fNorExec        GetRxData;

   
}sAppInfoType;




extern sAppInfoType*            App_Initialize(void);
extern void                     App_Start(void);
extern void                     App_SleepWakeup(void);




#endif

