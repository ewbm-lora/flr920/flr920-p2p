
//----------------------------------------------------------------------------
//    Project Name		: LoRa WAN
//----------------------------------------------------------------------------
//	File Name			: Main.h
//----------------------------------------------------------------------------
//	Processor			: STM32L071CZY    WLCSP
//	Compiler				: uVision V5.20.0.0
//	RF Transceiver	: SX1276
//	Frequency			:  917 ~ 928Mhz
//	Modulation			:  LoRa
//	Version				: H/W(1.3), LIB(1.01)
//	Company			: Wisol
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//	History
//	
//	DATE				Author			Version 		COMMENT
//	2018/06/01	KSH,CYH 		V1.00		Start
// 2016/07/31	 KSH,CYH		V1.01       	Modify : AFA, LRW 5D,Select Channel  Add : AU915, 
// 2016/08/01    KSH,CYH       V1.01         Add cmd: Channel Mask, Rx Delay1, Data rate, Join Accept Delay1, Repeater support, Activation, ABP setting value
//                                                                  Modify : JOIN_START, API_MODE                     
//----------------------------------------------------------------------------


#ifndef __MAIN_H
#define __MAIN_H

#include "stm32l0xx.h"




#endif

