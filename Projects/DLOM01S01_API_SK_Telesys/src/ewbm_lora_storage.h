/** 
  * @file     ewbm_lora_storage.h
  * @author   eWBM
  * @version  0.1
  * @date     9/17/2018
  * @brief    Key and etc information is stored into Flash, whole data is encrypt using Secure storage 
  * @attention
  *      Copyright(c) 2018 eWBM Co., Ltd. www.e-wbm.com
  *
  *       This file contains information that is proprietary to
  *      eWBM Co.,Ltd. and may not be distributed or copied
  *      without written consent from eWBM Co.,Ltd.
  *
  *      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  *      ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  *      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  *      IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  *      EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
  *      GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
  *      HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHERIN CONTRACT, STRICT LIABILITY, OR TORT
  *      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  * Brief history
  * -------------
  *  9/17/2018 : Draft by eWBM 
  */

#ifndef __EWBM_LORA_STORAGE_H__
#define __EWBM_LORA_STORAGE_H__

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>

/*! 
 * LoRa (Secure)Storage Item Type
 */
typedef enum eLoraStorageItemType
{
    LORA_U08_DEVEUI,
    LORA_U08_AES_PWD,
    LORA_U08_DBGL,
    LORA_U08_CHANNEL,
    LORA_U08_RATESET,
    LORA_U08_POWER,
    LORA_U08_NULLMODEM,
    LORA_U08_AES128,
    LORA_U08_HOP_LEVEL,
    LORA_U08_RECV_FORMAT,
    LORA_U16_PANID,
    LORA_U16_NODEID,
    LORA_U32_BAUDRATE,
    LORA_U32_HOP_DELAY,
    LORA_U32_STO_SIZE,
    LORA_MAX
    
} LoraStorageItemType;

#define EWBM_USER_STORAGE_START_ADDRESS 0x8080800


//--- exposed functions ----------------------------------------------------------
/**
  * @brief  Init. (Secure)Storage
  * @param  None
  * @retval None
  */
void ewbm_lora_storage_init(void);

/**
 * @brief  Read data(byte array) given LoraStorageItemType
 * @param  LoraStorageItemType item
 * @param  uint8_t* data
 * @retval bool True(Success) or Not
 */
bool ewbm_lora_storage_readb(const LoraStorageItemType item, uint8_t* data);

/**
 * @brief  Read data(word) given LoraStorageItemType
 * @param  LoraStorageItemType item
 * @param  uint32_t* data
 * @retval bool True(Success) or Not
 */
bool ewbm_lora_storage_readw(const LoraStorageItemType item, uint16_t* data);
bool ewbm_lora_storage_readdw(const LoraStorageItemType item, uint32_t* data);


/**
 * @brief  Write data(byte array) given LoraStorageItemType
 * @param  LoraStorageItemType item
 * @param  uint8_t* data
 * @retval bool True(Success) or Not
 */
bool ewbm_lora_storage_writeb(const LoraStorageItemType item, const uint8_t *data);

/**
 * @brief  Write data(word) given LoraStorageItemType
 * @param  LoraStorageItemType item
 * @param  uint32_t data
 * @retval bool True(Success) or Not
 */
bool ewbm_lora_storage_writew(const LoraStorageItemType item, const uint16_t data);
bool ewbm_lora_storage_writedw(const LoraStorageItemType item, const uint32_t data);


/**
 * @brief  Read data(byte array) from eeprom user space
 * @param  uint32_t eeprom address
 * @param  uint8_t* data
 * @param  uint32_t data length
 * @retval bool True(Success) or Not
 */
bool ewbm_user_storage_readb(const uint32_t addr, uint8_t* data, const uint32_t len);

/**
 * @brief  Write data(byte array) to eeprom user space
 * @param  uint32_t eeprom address
 * @param  uint8_t* data
 * @param  uint32_t data length
 * @retval bool True(Success) or Not
 */
bool ewbm_user_storage_writeb(const uint32_t addr, const uint8_t* data, const uint32_t len);

#endif /* __EWBM_LORA_STORAGE_H__ */
